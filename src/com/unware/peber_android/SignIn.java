package com.unware.peber_android;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pushbots.push.Pushbots;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

/**
 * Source: Android dev. template. (Des, 17th)
 * A login screen that offers login via email/password.
 */
public class SignIn extends Activity {
	
	// Tag untuk logging
	private final String TAG = this.getClass().getSimpleName();
	
	// Account manager
	private AccountManager _accountMgr = null;
	private SignIn this_context;
	private final String PEBER_ACCOUNT_TYPE = "com.unware.peber_android.account";

	// Asynctask untuk request
	private UserSigninTask mAuthTask = null;

	// UI references.
	private TextView textView_sign_message;
	private AutoCompleteTextView signin_username;
	private EditText signin_password;
	private View mProgressView;
	private View mLoginFormView;
	
	// Apps controller utk menghimpun variabel
	private AppsController aController;
	
	// Var untuk menentukan apakah proses sign in berhasl
	private boolean is_signin_success = false;
	private int found_user_id = 7;  // 7 : Dummy user 
	private int found_user_desc_id = 0;
	private String found_username = "";
	private String str_username = "";
	private String str_password = "";
	
	// GUI Text Needs
	private CharSequence loading_string =  "";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this_context = this;  // Context
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		
		// Insialisasi Account manager
		try {
			_accountMgr = AccountManager.get(this);
			Account[] accounts = _accountMgr
					.getAccountsByType(PEBER_ACCOUNT_TYPE);
			
			// Jika akun diperoleh dari sistem
			// maka, status sudah sign in. 
			if(accounts.length != 0) {
				Toast.makeText(this_context, "Already signed in! :)", Toast.LENGTH_LONG).show();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// GUI Texts initilization
		loading_string = getString(R.string.lable_loading);

		// Set up the login form.
		textView_sign_message = (TextView) this.findViewById(R.id.textView_sign_message); 
		signin_username = (AutoCompleteTextView) findViewById(R.id.signin_username);

		signin_password = (EditText) findViewById(R.id.signin_password);
		signin_password
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		Button mEmailSignInButton = (Button) findViewById(R.id.button_submit_signup);
		mEmailSignInButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});
		
		// Tombol sign up
		Button button_sign_up = (Button) findViewById(R.id.button_sign_up);
		button_sign_up.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(getApplicationContext(), SignUp.class));
			}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mProgressView = findViewById(R.id.login_progress);
		
		// Tangani extra dari Activity Sign Up
		// Ketika user selesai mendaftar, user harus login lagi.
		Bundle extras = getIntent().getExtras();
		if(null != extras && getIntent().getExtras().containsKey("password_plain")
				&& getIntent().getExtras().containsKey("username")) {
			String username_extra = extras.getString("username");
			String password_plain_extra = extras.getString("password_plain");
			
			// Langsung proses
			mAuthTask = new UserSigninTask();
			mAuthTask.execute(username_extra, password_plain_extra);
		} else {
			Log.d(TAG, "User must sign in manually");
		}
		
	}

	/**
	 * Sumber (Android Developer Sample Code)
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		signin_username.setError(null);
		signin_password.setError(null);

		// Store values at the time of the login attempt.
		String username = signin_username.getText().toString();
		String password = signin_password.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			signin_password.setError(getString(R.string.error_invalid_password));
			focusView = signin_password;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(username)) {
			signin_username.setError(getString(R.string.error_field_required));
			focusView = signin_username;
			cancel = true;
		} 

		if (cancel) {
			// Form belum di isi dengan baik dan benar
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			// showProgress(true);
			
			// Encrypt password
			Log.i(TAG, "Password (Plain): " + password);
			
			mAuthTask = new UserSigninTask();
			mAuthTask.execute(username, password);
			
			// request_signin(username, password);
		}
	}
	

	private boolean isPasswordValid(String password) {
		return password.length() > 4;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}


	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserSigninTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			textView_sign_message.setText(loading_string);
		}

		@Override
		protected String doInBackground(String... args) {

			// Ambil data from args
			str_username = args[0];
			str_password = args[1];
			
			// Atur format URL untuk masuk
			String url_user_prop = String.format("?username=%s&password=%s", str_username, str_password);		
			String url_user = aController.getUrl_user(url_user_prop);		
			Log.i(TAG, "URL Prop to send: " + url_user_prop);
			
			// OkHttp untuk akses API Django User
			OkHttpClient okHttp = new OkHttpClient();
			Request request = new Request.Builder()
				.url(url_user)
				.build();			
			Call call = okHttp.newCall(request);		
			call.enqueue(new Callback() {	
				
				@Override
				public void onResponse(com.squareup.okhttp.Response response)
						throws IOException {
					try {
						if(response.isSuccessful()) {
							String jsonText = response.body().string();
							
							Log.i(TAG, "Response okHttp: " + jsonText);
							JSONObject jsonObject = new JSONObject(jsonText);
							
							// Ambil data dengan label "results" (Menampung semua item dari API)
							JSONArray jarray_user = jsonObject.getJSONArray(aController.api_objects);
							
							// Memastikan jumlah respon data = 1
							if(jarray_user.length() == 1) {
								
								// Ambil index pertama dalam JSONArray User
								JSONObject user_object = jarray_user.getJSONObject(0);
								found_user_id = user_object.getInt("id");
								found_username = user_object.getString("username");
								
								// Cek jika id = 7, berarti data tidak ada (Akun NULL)
								if (found_user_id != 7){
									// User available
									Log.i(TAG, "Ditemukan username: " + found_username);
									
									// Mengambil ID User Desc berdasarkann ID Django User
									JSONArray user_desc_array = user_object.getJSONArray("userdescs");
									found_user_desc_id = user_desc_array.getInt(0);
									
									// Ubah id user desc aktif
									aController.setActive_user_id(found_user_desc_id);
									
									// Pastikan username respon sama dengan username request
									if(found_username.equals(str_username)) {
										// Untuk memastikan sistem muat data dari server jika user
										// baru login.
										aController.setUrl_next_news_page("");						
										
										// Daftar akun ke dalam Android Account Manager
										String account_name = found_username+"__"+found_user_desc_id;
										String account_pass = user_object.getString("password");
										Account account = new Account(
												account_name, 
												PEBER_ACCOUNT_TYPE);										
										boolean add_success = _accountMgr
												.addAccountExplicitly(account, account_pass, null);
										
										// Daftar PushBots Alias (Jan, 9)
										// Format alias: username__id_user (double underscore)
										Pushbots.sharedInstance().setRegStatus(true);
										Pushbots.sharedInstance().register(account_name, "None");
										
										if (add_success) {
											// Ganti status sign in menjadi true
											// sehingga bisa lanjut buka Home.
											is_signin_success = Boolean.TRUE;
										}										
										
									} else {
										is_signin_success = Boolean.FALSE;
									}
									
								}								
							}
							
						// Jika respon gagal.
						}  else {
							Log.e(TAG, "User tidak ada!.");								
						}
						
					} catch (IOException | JSONException e) {
						e.printStackTrace();
					} finally {
						if (is_signin_success) {
							// Jika user baru login, bersihkan data terlebih dahulu
							aController.clear_all_news_list();
							
							// Berikan tanda bahwa user baru saja masuk. (Jan 11)
							// Ini berguna untuk men-set tag PushBots pada saat Home.java
							// selesai request data Peber User Desc
							aController.is_currently_signed_in = true;
							
							// Buka Home
							startActivity(new Intent(getApplicationContext(), Home.class));	
							
						}  else {
							// Proses sign in tidak berhasil
							publishProgress("Username/password tidak dikenali");
						}
						
					}
				}
				
				

				@Override
				public void onFailure(Request req, IOException ex) {
					Log.e(TAG, "GAGAL (Failure) okHttp: " + req.body().toString());
					ex.printStackTrace();
					
				}
			});
			
			return "";
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			
			update_message(values[0]);
			
		}

		@Override
		protected void onPostExecute(final String s) {
			mAuthTask = null;
			showProgress(false);
			
//			if (success) {
//				startActivity(new Intent(getApplicationContext(), Home.class));
//			} else {
//				signin_password
//						.setError(getString(R.string.error_incorrect_password));
//				signin_password.requestFocus();
//			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	
	// Ubah pesan pada text view
	public void update_message(String s) {
		textView_sign_message.setText(s);
	}
}
