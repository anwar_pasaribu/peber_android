package com.unware.peber_android;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.unware.peber_android.R;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News;

public class ListHomeAdapter extends ArrayAdapter<News> implements Filterable {

	private final static int ITEM_LAYOUT = R.layout.item_list_home;
	private Context context;
	private List<News> lv_data_news;
	private List<News> all_news = new ArrayList<News>(); 
	NewsFilter news_filter;

	public ListHomeAdapter(Context context, List<News> news_list) {
		super(context, ITEM_LAYOUT, news_list);
		
		this.context = context;
		
		// List data yg akan ditampilkan
		this.lv_data_news = new ArrayList<News>();
		this.lv_data_news.addAll(news_list);
	}
	
	public void add(News news){
		this.lv_data_news.add(news);
	}

	@Override
	public void clear() {
		if(!this.lv_data_news.isEmpty()) {
			this.lv_data_news.clear();
		}
	}

	@Override
	public int getCount() {
		if(lv_data_news != null)
			return lv_data_news.size();
		else
			return 0;
		
	}

	@Override
	public News getItem(int position) {
		if(this.lv_data_news != null) {
			this.lv_data_news.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return this.lv_data_news.get(position).getId();
	}

	@Override
	public Filter getFilter() {
		if(news_filter == null)
			news_filter = new NewsFilter();
		
		return news_filter;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View itemView = null;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder;
		
		News news = lv_data_news.get(position);
		holder = new ViewHolder();
		
		if(!news.getNews_title().equals("footer")) {
			// Tampilan Normal List Item			
			itemView = inflater.inflate(ITEM_LAYOUT, parent, false);
			
			holder.textView_news_title = (TextView) itemView.findViewById(R.id.textView_news_title);
			holder.textView_news_corp = (TextView) itemView.findViewById(R.id.textView_news_corp);
			holder.imageView_news_image_hero = (ImageView) itemView.findViewById(R.id.imageView_news_image_hero);
			holder.textView_news_pub_date = (TextView) itemView.findViewById(R.id.textView_news_pub_date);
			
			// String news_summary = current_list.getNews_summary();
			String news_category = "";
			String news_publisher = "";
			String news_pub_date = "";
			if(news.getNews_corp() != null) {
				news_category = news.getNews_corp().getSource_category();  // e.g Kompas
				news_publisher = news.getNews_corp().getSource_publisher();  // e.g Kompas
				news_pub_date = Helper.getIndonesianDate(news.getNews_pub_date());
			}
			
			
			holder.textView_news_title.setText(news.getNews_title());
			holder.textView_news_corp.setText(String.format("%s (%s)", news_publisher, news_category));
			holder.textView_news_pub_date.setText(news_pub_date);
			
			// Download gambar pada list item (Home)
			if(!TextUtils.isEmpty(news.getNews_image_hero())) {
				// Picasso.with(context).setIndicatorsEnabled(true);
				Picasso.with(context)
				.load(news.getNews_image_hero())
				.placeholder(R.drawable.list_image_ph)
				.error(R.drawable.list_image_error)
				.resize(90, 90)
				.centerCrop()
				.into(holder.imageView_news_image_hero);			
			}
			
			itemView.setTag(holder);
			
		} else {  
			// Tampilan Footer
			itemView = inflater.inflate(R.layout.list_view_ended, parent, false);
			
			holder.textView_footer_title = (TextView) itemView.findViewById(R.id.textView_footer_title);
			holder.textView_footer_desc = (TextView) itemView.findViewById(R.id.textView_footer_desc);
			
			itemView.setTag(holder);
			
		} 
		
//		else {
//		
//			holder = (ViewHolder) itemView.getTag();
//		}
		
		
		return itemView;
	}
	
	
	private static class ViewHolder{
		protected ImageView imageView_news_image_hero;
		protected TextView textView_news_title, textView_news_corp, textView_news_pub_date;
		
		// View utk footer
		protected TextView textView_footer_title, textView_footer_desc;
	}
	
	private class NewsFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// Create a FilterResults object
            FilterResults results = new FilterResults();
            
            // Semua data berita
            all_news = AppsController.getInstance().get_all_news();
            
			// If the constraint (search string/pattern) is null
            // or its length is 0, i.e., its empty then
            // we just set the `values` property to the
            // original contacts list which contains all of them
            if (constraint == null || constraint.length() == 1) {
                results.values = lv_data_news;
                results.count = lv_data_news.size();
            }
            else {
                // Some search copnstraint has been passed
                // so let's filter accordingly
                ArrayList<News> filtered_news = new ArrayList<News>();
                
                // Format penulisan. 
                Locale locale = Locale.getDefault();

                // We'll go through all the contacts and see
                // if they contain the supplied string
                for (News news : all_news) {
                    if (news.getNews_title().toUpperCase(locale).contains( constraint.toString().toUpperCase(locale) )) {
                        // if `contains` == true then add it
                        // to our filtered list
                        filtered_news.add(news);
                    }
                }

                // Finally set the filtered values and size/count
                results.values = filtered_news;
                results.count = filtered_news.size();
            }

            // Return our FilterResults object
            return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			if(results.count != 0) {
				lv_data_news = (List<News>) results.values;
				notifyDataSetChanged();
			}
			
			
			
		}
		
	}  // End - News Filter
	

}
