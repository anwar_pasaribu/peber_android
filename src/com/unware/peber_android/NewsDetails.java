package com.unware.peber_android;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;
import com.unware.peber_android.control.CustomJsonObjectRequest;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News;
import com.unware.peber_android.model.News_Source;

public class NewsDetails extends Activity {
	
	private final String TAG = this.getClass().getSimpleName();
	private AppsController aController;
	private CustomJsonObjectRequest customJsonObjectRequest;
	private String tag_json_obj = "tag_get_news_by_id";
	
	private int news_id = 0;
	private News current_news = null;
	private String news_url;
	
	//Activity Widget
	private ProgressBar progressBar_detail;
	private ImageView imageView_news_image_hero;
	private TextView textView_detail_news_id,
			textView_detail_news_publisher, 
			textView_detail_news_pub_date, 
			textView_detail_news_summary,
			textView_detail_news_title, 
			textView_detail_news_content;
	
	private Button btn_open_site;
	
	// Image Loader dengan Google Volley
	ImageLoader imageLoader = AppsController.getInstance().getImageLoader();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_details);
		
		//Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// Ambil widget dari layout
		initializeWidget();
		
		// Data news_id dari notif.
		Bundle extras = getIntent().getExtras();
		if(null != extras && getIntent().getExtras().containsKey("news_id")) {
			// Extras yang diperoleh dari Home.java
			// Karena data dari Pushbots dianggap string
			news_id = Integer.valueOf(extras.getString("news_id"));  
			
			// Kirim url dengan news_id
			// Sekaligus menampilkan konten
			// Argumen kedua url_prop tidak ada
			request_json_object(aController.getUrl_news(news_id, ""));
		}
		
		if(current_news != null) {
			setWidgetContent();
		}
		
	}
	

	private void setWidgetContent() {
		
		//setTitle("News Details");
		
		news_url = current_news.getNews_url();
		
		String str_format = "ID %d";
		
		String str_news_id = String.format(str_format, current_news.getId());
		String news_title = current_news.getNews_title();
		String news_pub_date = Helper.getIndonesianDate(current_news.getNews_pub_date());
		String news_corp_publisher = current_news.getNews_corp().getSource_publisher();
		String news_summary = current_news.getNews_summary();
		String news_content = current_news.getNews_content();
		
		// Load image with Picasso
		// Manambah opsi resize untuk membuat foto mengisi ruang ImageView (Jan, 7th)
		if(!TextUtils.isEmpty(current_news.getNews_image_hero())) {
			Picasso.with(getApplicationContext())
				.load(current_news.getNews_image_hero())
				.centerCrop()
				.resize(imageView_news_image_hero.getMeasuredWidth(), imageView_news_image_hero.getMeasuredHeight())
				.placeholder(R.drawable.img_placeholder)  // Gambar sebelum gambar dimuat
				.into(imageView_news_image_hero);		
		}
		
		textView_detail_news_id.setText(str_news_id);
		textView_detail_news_title.setText(Html.fromHtml(news_title));
		textView_detail_news_publisher.setText(news_corp_publisher);
		textView_detail_news_pub_date.setText(news_pub_date);
		textView_detail_news_summary.setText(news_summary);
		textView_detail_news_content.setText(news_content);
		
		// Listener untuk tombol pembuka URL
		btn_open_site.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent news_url_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news_url));
				startActivity(news_url_intent);
			}
		});
		
		// Hilangkan Progress Bar
		progressBar_detail.setVisibility(View.GONE);
		
	}


	private void initializeWidget() {
		
		// Progress Bar
		progressBar_detail = (ProgressBar) findViewById(R.id.progressBar_detail);
		
		imageView_news_image_hero = (ImageView) findViewById(R.id.imageView_news_image_hero_detail);
		textView_detail_news_title = (TextView) findViewById(R.id.textView_detail_news_title);
		textView_detail_news_id = (TextView) findViewById(R.id.textView_detail_news_id);
		textView_detail_news_publisher = (TextView) findViewById(R.id.textView_detail_news_publisher);
		textView_detail_news_pub_date = (TextView) findViewById(R.id.textView_detail_news_pub_date);
		textView_detail_news_summary = (TextView) findViewById(R.id.textView_detail_news_summary);
		textView_detail_news_content = (TextView) findViewById(R.id.TextView_detail_news_content);
		
		btn_open_site = (Button) findViewById(R.id.btn_open_news_site);
		
		// Kosongkan teks pada view
		textView_detail_news_id.setText("");
		textView_detail_news_title.setText("");
		textView_detail_news_publisher.setText("");
		textView_detail_news_pub_date.setText("");
		textView_detail_news_summary.setText("");
		textView_detail_news_content.setText("");
		
		
		
	}

	
	private void request_json_object(String url) {	
		Log.i(TAG, "URL: " + url);
		
		Map<String, String> data_request = new HashMap<String, String>();
		data_request.put("id_user", "1");
		
		customJsonObjectRequest = new CustomJsonObjectRequest(
				Method.GET,
				url, 
				data_request, 
				new Response.Listener<JSONObject>() {	
					
					@Override
					public void onResponse(JSONObject response) {
						//Mengolah data yang sudah di dapat dari Django API						
						process_news_json_object(response);
					}
				}, new Response.ErrorListener() {	
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.toString());
						error.printStackTrace();
					}
				}){};
				customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
		
	
		// Adding request to request queue (Menjalankan Request)
		// TODO Lakukan pemeriksaan cache sehingga tidak dilakukan request ke server lagi.
		AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_json_obj );
	}
	
	private void process_news_json_object(JSONObject jsonObject) {
		News news_item_temp = null;
		try {
					
			// Olah data news_corp (News_Source)
			JSONObject nc_object = jsonObject.getJSONObject("news_corp");
			int news_source_id = nc_object.getInt("id");
			String source_category = nc_object.getString("source_category");
			String source_publisher = nc_object.getString("source_publisher");
			String source_url = nc_object.getString("source_url");
			
			News_Source news_corp = new News_Source(news_source_id, source_publisher, source_category, source_url);
			
			// Set data aktif berdasarkan ID yg ditentukan.
			news_item_temp = new News(
					jsonObject.getInt("id"),
					jsonObject.getString("news_url"),
					jsonObject.getString("news_title"),
					jsonObject.getString("news_content"),
					news_corp,
					jsonObject.getString("news_summary"),
					jsonObject.getString("news_pub_date"),
					jsonObject.getString("news_image_hero")
					);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		this.current_news = news_item_temp;
		
		// Tampilkan data pada view
		setWidgetContent();
		
	}




	//////////////////////////////////////////////////////////
	/////////////////////////OPTION MENU//////////////////////
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.news_details, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
	////////////////////////OPTION MENU - END//////////////////

}
