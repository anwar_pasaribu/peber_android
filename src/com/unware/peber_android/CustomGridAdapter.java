package com.unware.peber_android;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.unware.peber_android.R;
import com.unware.peber_android.model.News_Source;

public class CustomGridAdapter extends BaseAdapter {

	private final static int GRID_ITEM_LAYOUT = R.layout.grid_item;
	private Context context;
	private List<News_Source> all_news_source_data;

	// Constructor to initialize values
	public CustomGridAdapter(Context context, List<News_Source> grid_data) {

		this.context = context;
		this.all_news_source_data = new ArrayList<News_Source>();
		this.all_news_source_data.addAll(grid_data);
		
	}
	
	public void add(News_Source ns){
		this.all_news_source_data.add(ns);
	}
	
	public void clear() {
		if(!this.all_news_source_data.isEmpty()) {
			this.all_news_source_data.clear();
		}
	}
	
	public int size() {
		if(!this.all_news_source_data.isEmpty()) {
			return this.all_news_source_data.size();
		}
		
		return 0;
	}


	@Override
	public int getCount() {
		// Number of times getView method call depends upon gridValues.length
		return all_news_source_data.size();
	}

	@Override
	public Object getItem(int position) {
		if(!this.all_news_source_data.isEmpty()) {
			return this.all_news_source_data.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		// Id yg data ns yg dikirim saat aktivasi onClick listener
		return all_news_source_data.get(position).getId();
	}

	// Number of times getView method call depends upon gridValues.length

	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		View itemView = null;
		LayoutInflater 	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder;

		News_Source current_data = all_news_source_data.get(position);
		holder = new ViewHolder();

		if(current_data.getId() != -1) {
			itemView = inflater.inflate(GRID_ITEM_LAYOUT, parent, false);
	
			// Initialize widget
			holder.textView_source_category = (TextView) itemView.findViewById(R.id.grid_item_news_category);
			holder.textView_source_publihser = (TextView) itemView.findViewById(R.id.grid_item_label_news_publisher);
			holder.imageView = (ImageView) itemView.findViewById(R.id.grid_item_image);
			
			// Gambar khusus untuk grid tambah news_source
			if(current_data.getId() == 0){
				holder.imageView.setImageResource(R.drawable.btn_add);
				holder.textView_source_category.setText("Add More");
				holder.textView_source_publihser.setText("");
				
			} else if (current_data.getId() == 2){
				// Grid item kosong (untuk menutupi glitch*)
				holder.imageView.setVisibility(View.GONE);
				holder.textView_source_category.setText("");
				holder.textView_source_publihser.setText("");  
			} else {
				holder.imageView.setImageResource(R.drawable.tag_off);
				holder.textView_source_category.setText(current_data.getSource_category());
				holder.textView_source_publihser.setText("");  // Publisher tidak ada lagi (26 Nov, 15)
			}
			
			itemView.setTag(holder);
		
		} 
		
		return itemView;
	}

	// //////////////////////////////////////////////////////////
	// Statik class utk mempercepat performa
	// apabila list sudah besar
	private static class ViewHolder {
		// Widget utk header
		protected TextView textView_source_category, textView_source_publihser;
		protected ImageView imageView;

	}
	// /////////////////////////////////////////////////////////

}