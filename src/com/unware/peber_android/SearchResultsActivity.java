package com.unware.peber_android;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.unware.peber_android.R;
import com.unware.peber_android.control.CustomJsonObjectRequest;
import com.unware.peber_android.model.News;

public class SearchResultsActivity extends Activity {
	private final String TAG = this.getClass().getSimpleName();

	private TextView tv_search_result;
	private ListView lv_search_res;
	private List<News> search_res_list = new ArrayList<News>();
	private ListHomeAdapter adapter_search_res = null;
	
	//URL Utk akses data API
	private String url_news;
	
	// View more data
	private View view_load_more;
	
	// Status Scroll yg mencapai bawah
	private boolean is_bottom_reached = false;
	
	// Izinkan scroll setelah touch dimulai.
	// Karena normalnya, onScroll akan dijalankan pada onCreate
	private boolean user_scrolled = false;
	
	private AppsController apps;
	private CustomJsonObjectRequest customJsonObjectRequest;
	
	// Tag utk akses data dari API
	private final String api_objects = "results";
	private boolean LOG = true;  // Tampilkan log pada console
	private boolean pDialog_run_once = false;
	
	boolean loadingMore = false;
	
	private final int news_process = 2;
	
	// Info Halaman
	private int news_count = 0;
	private int total_page = 0;
	private int news_page = 0;
	private String next_news_page = null;
	
	private String query = null; 
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_results);
		
		Log.v(TAG, "OnCreate");
		
		// Application Controller
		apps = (AppsController) getApplicationContext();
		
		// Text View keterangan query user
		tv_search_result = (TextView) this.findViewById(R.id.textView_search_results);
		
		// List View yang akan menampung data berita
		lv_search_res = (ListView) this.findViewById(R.id.listView_list_search_res);
		
		// Tampilan Load more data pada akhir list view
		view_load_more = ((LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.list_view_load_more, lv_search_res, false);
		
		// Mengatur List View		
		lv_search_res.addFooterView(view_load_more);  // Tampilan loading 
		adapter_search_res = new ListHomeAdapter(this, search_res_list);
		lv_search_res.setAdapter(adapter_search_res);
		populate_list_view_listener();  // Listener pada list view	
		
		// Olah query yang masuk
		load_query_data(getIntent());

	}
	
	
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.v(TAG, "onResume");
		
		// Scrolling LV ke atas
		lv_search_res.setSelectionFromTop(0, 0);  
	}



	/** 
	 * Dipanggil setelah Request semua data API dan data yang respon ada.
	 */
	private void populate_list_view_listener() {
		
		lv_search_res.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {				
				
				if (id != -1) {  // Kasus error ketika user tap "Loading..."
					
					News selected_data = (News) search_res_list.get(position);
					String news_id = String.valueOf(selected_data.getId());
					
					if (LOG) Toast.makeText(getApplicationContext(), "Id Berita: "+news_id, Toast.LENGTH_SHORT).show();
					
					// Buka Activity News Details
					Intent intent_detail_kamar = new Intent(getApplicationContext(), NewsDetails.class);
					intent_detail_kamar.putExtra("news_id", news_id);
					startActivity(intent_detail_kamar);
					
				} else {
					if (LOG) Toast.makeText(getApplicationContext(), "No news found!", Toast.LENGTH_SHORT).show();
				}
				
				
				
			}
		});
		
		lv_search_res.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState){
				// Untuk menghindari request pada saat onCreate
				if(scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
					user_scrolled = true;
				}
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {	
				
				int lastInScreen = firstVisibleItem + visibleItemCount;	
				
				if(lastInScreen == totalItemCount 
						&& !(loadingMore)
						&& !(is_bottom_reached)  // Kondisi awal false; bwa
						&& user_scrolled
						&& next_news_page != null) {
					
					// Ubah status is_bottom_reached supaya request dipanggil sekali.
					// Diganti lagi setelah data selesai dikelola.
					is_bottom_reached = true;
					
					// Jalankan request next_url
					if(news_page != total_page) {
						request_json_object(
							next_news_page, 
							news_process, 
							true);  // Tidak menampilkan Progres Dialog lagi.
					} else {
						// Jika tidak ada lagi link url data selanjutnya
		    			is_bottom_reached = true;  // Akhir data sudah diterima
		    			Toast.makeText(getApplicationContext(), "Last news reached", Toast.LENGTH_LONG).show();
		    			lv_search_res.removeFooterView(view_load_more);
					}						
						
				} 
				
			}
		});
		
	}
	

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		load_query_data(intent);
	}

	
	private void load_query_data(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			
			// Kata kunci pencarian.
			query = intent.getStringExtra(SearchManager.QUERY);

			/**
			 * Use this query to display search results like 1. Getting the data
			 * from SQLite and showing in listview 2. Making webrequest and
			 * displaying the data For now we just display the query only
			 */
			tv_search_result.setText("Sedang mencari \"" + query + "\"...");
			
			
			// URL Prop pencarian
			String search_prop = "?title=%s";
			
			// Ubah ke URL safe string.
			try {
				query = URLEncoder.encode(query, "UTF-8");
				search_prop = String.format(search_prop, query);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			// Generate url request berita dengan URL Properties judul berita.
			url_news = apps.getUrl_news(0, search_prop);
			
			// Request berita dengan query. Untuk mendapatkan berita berdasarkan judul.
			request_json_object(url_news, news_process, pDialog_run_once);
			
			// Ubah Apps Icon
			ActionBar actionbar = getActionBar();
			actionbar.setIcon(R.drawable.action_search);

		} else {
			Toast.makeText(getApplicationContext(), "Sesuatu tidak normal terjadi", Toast.LENGTH_LONG).show();
		}

	}
	
	
	private void request_json_object(String url, final int process_id, final boolean run_once) {	
		Log.i(TAG, "Request URL: " + url);
		
		// [BELUM DIPAKAI] Data yg akan dikirim ke server.
		Map<String, String> data_request = new HashMap<String, String>();
		data_request.put("id_user", "1");
		
		customJsonObjectRequest = new CustomJsonObjectRequest(
			Method.GET,
			url, 
			data_request, 
			new Response.Listener<JSONObject>() {	
				
				@Override
				public void onResponse(JSONObject response) {
					
					// Langsung mengolah data dari API
					process_news_and_populate_list_view(response);
					
				}
			}, new Response.ErrorListener() {	
				@Override
				public void onErrorResponse(VolleyError error) {
					VolleyLog.e(TAG, "Error: " + error.toString());
					error.printStackTrace();
				}
			}
		){};
				
		
		customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
		AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, "tag_news_search");
			
		
	}
	
	
	/**
	 * Mengolah data berita (JSONObject) yang diperoleh dari Django API. 
	 * Kemudian menampilkan ListView.
	 * @param resJsonObject: Json Object data berita
	 */
	private void process_news_and_populate_list_view(JSONObject resJsonObject) {
		
		try {
    		    		
    		//Jika header di JSON Object berisi "objects"
    		if( !resJsonObject.isNull(api_objects) ) {
    			
    			int i = 0;
    			
	    		JSONObject jsonResponse 		= new JSONObject(resJsonObject.toString());
	    		
	    		// Mendapatkan next dan prev url
	    		this.next_news_page = jsonResponse.getString("next");
	    		
	    		// Banyak berita
	    		this.news_count = jsonResponse.getInt("count");
	    		
	    		// Menentukan banyak halaman berita
	    		if(this.news_count % 10 != 0) {
	    			this.total_page = Math.round(this.news_count/10)+1;
	    		} else {
	    			this.total_page = Math.round(this.news_count/10);
	    		}
	    		
	    		// Array of news items
	    		JSONArray jArray_news_data 		= jsonResponse.getJSONArray(api_objects);    		

	    		int jArray_news_data_length 	= jArray_news_data.length();
	    		int news_list_size_old 			= this.search_res_list.size();
	    		
	    		int news_list_size_new = jArray_news_data_length;
	    		
	    		if(this.next_news_page == null) {
	    			// Jika tidak ada lagi link url data selanjutnya
	    			is_bottom_reached = true;  // Akhir data sudah diterima
	    			Toast.makeText(getApplicationContext(), "Last news reached", Toast.LENGTH_LONG).show();
	    			
	    			ListView listView = (ListView) findViewById(R.id.listView_list_home);
	    		    listView.removeFooterView(view_load_more);

	    		}
	    		else {
	    			// Jika data berita (aController) != data berita datang (Network)
	    			if(news_list_size_new > news_list_size_old) {		
	    				// Penambahan data dimulai dari index terakhir data lama
						for (i = news_list_size_old; i < jArray_news_data_length; i++ ) {							
							JSONObject jsonObject = jArray_news_data.getJSONObject(i);
							News news = apps.create_news_item(jsonObject);
							search_res_list.add(news);
							adapter_search_res.add(news);
						}
						
						adapter_search_res.notifyDataSetChanged();
	    			
	    			} else {  // if( news_list_size_new < news_list_size_old ) 	    				
	    				// Jika data yang baru masuk lebih sedikit,
	    				// langsung tambahkan data.
	    				for (i = 0; i < news_list_size_new; i++ ) {							
							JSONObject jsonObject = jArray_news_data.getJSONObject(i);
							News news = apps.create_news_item(jsonObject);
							search_res_list.add(news);
							adapter_search_res.add(news);
						}
	    				
	    				adapter_search_res.notifyDataSetChanged();
	    				
	    			}		    			
	    		}
	    		
	    		Log.i(TAG, String.format("Ukuran Data berita baru: %d\nUkuran Data Lama:%d, "
	    				+ "\nNews Count: %d, Total pages: %d, Current page: %d",
	    				news_list_size_new, news_list_size_old,
	    				this.news_count, this.total_page, (this.news_page + 1)
	    				));
	    		
	    		// Update text view jika sudah selesai parsing data.
	    		if (news_count >= 1) {
	    			tv_search_result.setText("Hasil untuk \"" + query + "\",\nDitemukan " + news_count + " berita." );
	    		} else {
	    			tv_search_result.setText("Tidak ada berita \"" + query + "\".");
	    			lv_search_res.removeFooterView(view_load_more);
	    			
	    		}
				
				// Scrolling LV ke atas
				lv_search_res.setSelectionFromTop(0, 0);
				
    		} 			
			
		} catch (Exception e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {
			
			if (this.next_news_page != null) {
				// Jika next page sudah masih ada lagi, diasumsikan akhir list belum dapat.
				is_bottom_reached = false;
				
			} else {
				is_bottom_reached = true;
				Toast.makeText(getApplicationContext(), "Last Page", Toast.LENGTH_LONG).show();
			}
			
			// Menambah halamn setiap kali berhasil parsing data News
			this.news_page += 1;
			
		}
		
	}
	
	
}
