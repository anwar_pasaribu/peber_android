package com.unware.peber_android.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.unware.peber_android.model.News_Source;

public class Helper {

	public Helper() {}
	
	
	public static String generate_news_url_properties(List<News_Source> user_ns) {
		StringBuilder url_prop_news = new StringBuilder();
		String res_url_prop_format = "?news_category=%s";
		String res_url_prop = null;
		String sep = "";
		for(News_Source ns: remove_ns_duplicate(user_ns)) {
			// Atur URL Prop (Category berita yg dipilih)
			url_prop_news.append(sep);
			url_prop_news.append(ns.getSource_category());
			sep = ",";
		}
		
		// Ubah ke URL safe string.
		try {
			res_url_prop = URLEncoder.encode(url_prop_news.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return String.format(res_url_prop_format, res_url_prop);
	}
	
	
	/**
	 * Menghasilkan List News Source tanpa duplikasi kategori berita.
	 * @param user_ns : List News Source
	 * @return Normalized News Source
	 */
	public static List<News_Source> remove_ns_duplicate(
			List<News_Source> user_ns) {
		
		// 0. Sediakan list penampung NS tanpa categori duplikat.
		List<News_Source> normalized_user_ns = new ArrayList<News_Source>();
		
		// 1. Ambil Source Category sebagai pembanding.
		List<String> news_categories = new ArrayList<String>();
		for(News_Source ns: user_ns) {
			String teks = ns.getSource_category();
			news_categories.add(teks);
		}
		
		// 2. Hilangkan duplikat dalam list Source Category.
		Set<String> set = new HashSet<String>(news_categories);
		ArrayList<String> unique_categories_list = new ArrayList<String>(set);
		
		// 3. Urutkan kategori A-Z
		Collections.sort(unique_categories_list);
		
		// 4. Ambil kembali News Source berdasarkan kategori tanpa duplikat.
		for(String str: unique_categories_list){
			for(News_Source ns : user_ns) {
				if(ns.getSource_category().equals(str)) {
					normalized_user_ns.add(ns);
					break;
				}
			}
		}
		
		return normalized_user_ns;
	}
	
	
	/**
	 * Membuat tanggal dengan format YYYY-MM-DD. 
	 * @return String tanggal dengan format MySQL.
	 */
	public static String getMySQLDateFormat() {
		
		//Membuat string tanggal sekarang
		Calendar now = Calendar.getInstance();
		int month = now.get(Calendar.MONTH) + 1;
		int day = now.get(Calendar.DAY_OF_MONTH);
		int year = now.get(Calendar.YEAR);
		
		return year+"-"+month+"-"+day;
		
	}
	
	/**
	 * Mendapatkan tanggal dengan Format Regional Indonesia.
	 * @param mysql_date_format: String tanggal (YYYY-MM-DD)
	 * @return String tanggal e.g 20 Mei
	 */
	public static String getIndonesianDate(String mysql_date_format) {
		
		Calendar now = Calendar.getInstance();
		int nowYear = now.get(Calendar.YEAR);
		
		// Potong sesuai T
		String[] time_items = mysql_date_format.split("T");
		
		String[] date_string = time_items[0].split("-");
		
		// String waktu
		String time_string = time_items[1];
		
		String tahun = date_string[0];
//		String bulan = getMonthName(date_string[1]);
		String bulan = date_string[1];
		String tgl	 = date_string[2];
		
//		if(Integer.parseInt(tahun) == nowYear)
//			tahun = "";  // Tahun sekarang tidak dicantumkan.
		
//		return tgl + " " + bulan + " " + tahun + " - " + time_string.substring(0, 5) + " WIB";
		return tgl + "/" + bulan + "/" + tahun + " - " + time_string.substring(0, 5) + "";
	}

	private static String getMonthName(String substring) {
		
		switch (Integer.parseInt(substring)) {
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "Mei";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Agust";
		case 9:
			return "Sept";
		case 10:
			return "Okt";
		case 11:
			return "Nov";
		case 12:
			return "Des";
		default:
			break;
		}
		
		return null;
	}

}
