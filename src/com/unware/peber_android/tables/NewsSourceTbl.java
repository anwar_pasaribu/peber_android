package com.unware.peber_android.tables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


@Table(name = "news_source")
public class NewsSourceTbl extends Model{
    @Column(name = "ns_id")
	public int ns_id;    
    @Column(name = "source_publisher")
	public String source_publisher;
    @Column(name = "source_category")
    public String source_category;
    @Column(name = "source_url")
    public String source_url;
	
	public NewsSourceTbl(){
		 super();
	}
	
	public NewsSourceTbl(int id, String source_publisher, String source_category,
			String source_url) {
		this.ns_id = id;
		this.source_publisher = source_publisher;
		this.source_category = source_category;
		this.source_url = source_url;
	}

}
