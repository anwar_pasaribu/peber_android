package com.unware.peber_android.tables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "peber_user")
public class UserTbl extends Model {
	@Column(name = "user_id")
	public int user_id;
	@Column(name = "username")
	public String username;
	@Column(name = "email")
	public String email;
	@Column(name = "first_name")
	public String first_name;
	@Column(name = "last_name")
	public String last_name;
	@Column(name = "pushbots_regid")
	public String pushbots_regid;
	@Column(name = "pushbots_tags")
	public String pushbots_tags;
	@Column(name = "pushbots_alias")
	public String pushbots_alias;

	public UserTbl() {
		super();
	}
	
	public UserTbl(int id, String username, String email, String first_name,
			String last_name) {
		this.user_id = id;
		this.username = username;
		this.email = email;
		this.first_name = first_name;
		this.last_name = last_name;
	}

}
