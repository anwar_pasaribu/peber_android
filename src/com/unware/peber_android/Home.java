package com.unware.peber_android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.pushbots.push.Pushbots;
import com.unware.peber_android.control.CustomJsonObjectRequest;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News;
import com.unware.peber_android.model.News_Source;
import com.unware.peber_android.model.User;
import com.unware.peber_android.model.UserDesc;
import com.unware.peber_android.tables.NewsSourceTbl;

public class Home extends Activity {
	
	private final String TAG = this.getClass().getSimpleName();
	private ProgressDialog pDialog;
	private String tag_news_req = "tag_news_request";
	private String tag_ns_req = "tag_news_source_request";
	private String tag_user_desc_req = "tag_user_desc_req";
	
	// Account manager (Jan, 9)
	private AccountManager _accountMgr = null;
	private Home this_context;
	private final String PEBER_ACCOUNT_TYPE = "com.unware.peber_android.account";
	
	
	//URL Utk akses data API
	private String url_news;
	private String url_user; // Hanya satu data (Anggun)
	private String url_news_source; // Hanya satu data (Anggun)
	private String url_prop_news = null;
	
	//Process ID utk menentukan data jsonArray yg akan diproses
	private final int user_desc_process = 1;
	private final int news_process = 2;
	private final int news_source_process = 3;
	
	private AppsController aController;
	private CustomJsonObjectRequest customJsonObjectRequest;
	
	// Tag utk akses data dari API
	private final String api_objects = "results";
	private boolean LOG = true;  // Tampilkan log pada console
	private boolean pDialog_run_once = false;

	// List view
	private ListView lv_home;
	private ListHomeAdapter custom_listview_adapter = null;
	
	// Untuk kontrol data API
	// News Page pindah ke Apps Controller
	boolean loadingMore = false;
	private View view_load_more;
	private View view_list_ended;
	
	// Status Scroll yg mencapai bawah
	private boolean is_bottom_reached = false;
	
	// Izinkan scroll setelah touch dimulai.
	// Karena normalnya, onScroll akan dijalankan pada onCreate
	private boolean user_scrolled = false;
	
	//Menyimpan posisi scroll listView
	private int lastViewedPosition;
	private int topOffset;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.v(TAG, "onCreate called");
		
		setContentView(R.layout.activity_home);
		
		// Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// ListView
		lv_home = (ListView) findViewById(R.id.listView_list_home);
		
		// Tampilan Load more data pada akhir list view
		view_load_more = ((LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.list_view_load_more, lv_home, false);
		
		// Inisisalisasi Pushbots
		// Pushbots register pada SignIn.java
		// PushBots unregister pada Setting ketika user tekan tombol sign out.
		// Set Alias berada pada SignIn.java (Jan, 9)
		// Set Tags berada pada Settings.java (Jan, 9)
		Pushbots.sharedInstance().init(this);
		
		//Inisialisasi Progress Dialog Box
		pDialog = new ProgressDialog(Home.this);
		pDialog.setMessage("Loading data. \nIP: " + aController.getIp_address());
		pDialog.setCancelable(true);
		
		// Mengatur List View		
		lv_home.addFooterView(view_load_more);  // Tampilan loading 
		custom_listview_adapter = new ListHomeAdapter(Home.this, aController.get_all_news());
		lv_home.setAdapter(custom_listview_adapter);
		lv_home.setTextFilterEnabled(true);		
		populate_list_home_listener();  // Listener pada list view
		
		Log.i(TAG, "User Desc ID: "+ aController.getActive_user_id());
		
		// Memperoleh data dari SQLite (Jan, 7)
		ArrayList<NewsSourceTbl> news_sources_aa = NewsSourceTbl.all(NewsSourceTbl.class);
		
		// URL API yang akan direquest
		// 1. URL user_desc dengan ID User Desc (default=3, null)
		url_user = aController.getUrl_user_desc(aController.getActive_user_id());
		// 2. URL news_source dengan param 0 (all NS data)
		url_news_source = aController.getUrl_news_source(0);
		
		if(aController.getUrl_next_news_page().equals("")) {
			// FRESH LOAD
			// 1. Pastikan tidak ada data News pada Apps Controller
			aController.clear_all_news_list();
			custom_listview_adapter.clear();
			custom_listview_adapter.notifyDataSetChanged();
			
			// Tiga request masing-masing News, News_Source, dan User_Desc pertama kali.
			// 2. Request user_desc, untuk memperoleh News_Source yg dipilih user.
			request_json_object(url_user, user_desc_process, pDialog_run_once);
			// 3. Request berita (news) sekaligus Populate ListView. 
			//    Berada pada proses parsing data User Desc. 
			//	  Karena data barita yg diambil, berdasarkan data User Desc.
			// 4. Request news_source.
			//    Sebelum melakukan req, periksa apakah sudah ada dalam sqlite dan batalkan jika sudah ada.
			if(news_sources_aa.size() == 0) {
				request_json_object(url_news_source, news_source_process, pDialog_run_once);
			} else {
				populate_sqlite_ns();
			}
			
		}
		
	}


	@Override
	public void onResume() {
		super.onResume();
		Log.v(TAG, "onResume called");
		
		// Kembalikan lokasi scroll list view.
		// Setelah view selesai dibuat, kemudian scroll ke posisi terkahir listVew
		// Data posisi terkahir disimpan dalam Apps Controller.
		lv_home.setSelectionFromTop(
				aController.listHome_lastViewedPosition, 
				aController.listHome_topOffset);  // From PasaribuStore Apps
		
		// In case user tekan Back Button instead Up Navigation.
		// Jika user ubah kategori yg dipilih.
		// is_changed_main_news_url di ubah di Setting.java.
		if(aController.is_changed_main_news_url) {
			Log.i(TAG, "URL Utama berubah, load berita dilukakan kembali.");
			
			aController.news_page = 0;  // Pastikan dimulai dari halaman pertama.
			aController.is_changed_main_news_url = false;  // Main URL sudah diset sebagai nilai default.
			
			// RESET List View beserta data terkait
			lv_home.addHeaderView(view_load_more);
			aController.clear_all_news_list();
			
			// RESET Adapter Home List View
			custom_listview_adapter.clear();
			custom_listview_adapter.notifyDataSetChanged();
			
			// Request data baru dengan kategori pilihan baru user.
			request_json_object(aController.getMain_news_url(), news_process, pDialog_run_once);
		}
		
		
		// Insialisasi Account manager
		try {
			_accountMgr = AccountManager.get(this);
			Account[] accounts = _accountMgr
					.getAccountsByType(PEBER_ACCOUNT_TYPE);
			
			// ID Peber User Desc yang sedang aktif
			int active_udesc_id = aController.getActive_user_id();
			
			// Jika Peber user belum terdaftar dalam Account Manager karena baru 
			// saja Sign Up.
			if(accounts.length == 0 && active_udesc_id != 3) {
				
				Toast.makeText(this_context, "Already signed in! But not registered on Account Manager", Toast.LENGTH_LONG).show();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		// Simpan lokasi skroll List View
		lastViewedPosition = lv_home.getFirstVisiblePosition();
		View v = lv_home.getChildAt(0);
		topOffset = (v==null) ? 0 : v.getTop();
		
		aController.listHome_lastViewedPosition = lastViewedPosition;
		aController.listHome_topOffset = topOffset;
		
	}
	

	/** 
	 * Dipanggil setelah Request semua data API dan data yang respon ada.
	 */
	private void populate_list_home_listener() {
		
		lv_home.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {				
				
				if (id != -1) {  // Kasus error ketika user tap "Loading..."
					
					String news_id = String.valueOf(id);
					
					if (LOG) Toast.makeText(Home.this, "Id Berita: "+news_id, Toast.LENGTH_SHORT).show();
					
					Intent intent_detail_kamar = new Intent(getApplicationContext(), NewsDetails.class);
					intent_detail_kamar.putExtra("news_id", news_id);
					startActivity(intent_detail_kamar);
					
				} else {
					if (LOG) Toast.makeText(Home.this, "No news found!", Toast.LENGTH_SHORT).show();
				}
				
				
				
			}
		});
		
		// Bagian untuk memuat data selanjutnya (23 Nov 15)
		lv_home.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState){
				// Untuk menghindari request pada saat onCreate
				if(scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
					Home.this.user_scrolled = true;
				}
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {	
				
				int lastInScreen = firstVisibleItem + visibleItemCount;
				
				String next_page_url = aController.getUrl_next_news_page();
				
				if(lastInScreen == totalItemCount 
						&& !(loadingMore)
						&& !(is_bottom_reached)  // Kondisi awal false; bwa
						&& user_scrolled
						&& next_page_url != null) {
					
					// Ubah status is_bottom_reached supaya request dipanggil sekali.
					// Diganti lagi setelah data selesai dikelola.
					is_bottom_reached = true;
					
					// Jalankan request next_url
					if(aController.news_page != aController.total_page) {
						Home.this.request_json_object(
							next_page_url, 
							Home.this.news_process, 
							true);  // Tidak menampilkan Progres Dialog lagi.
					} else {
						// Jika tidak ada lagi link url data selanjutnya
		    			is_bottom_reached = true;  // Akhir data sudah diterima
		    			Toast.makeText(Home.this, "Last news reached", Toast.LENGTH_LONG).show();
		    		    Home.this.lv_home.removeFooterView(view_load_more);
		    		    Home.this.custom_listview_adapter.add(new News(-1, "", "footer", "", null, "", "", ""));
		    		    Home.this.custom_listview_adapter.notifyDataSetChanged();
					}						
						
				} 
				
			}
		});
		
		
	}



	private void request_json_object(String url, final int process_id, final boolean run_once) {	
		Log.i(TAG, "Request URL: " + url);
		
		// Tampilkan Progress Dialog jika belum pernah tampil
		// Pemanggilan pertama fungsi ini (request_json_object),
		// membuat nilai pDialog_run_once = true. Sehingga progress
		// dialog akan tetap terlihat.
		if(!run_once) {
			pDialog.show();
			this.pDialog_run_once = true;
		}
		
		// [BELUM DIPAKAI] Data yg akan dikirim ke server.
		Map<String, String> data_request = new HashMap<String, String>();
		data_request.put("id_user", "1");
		
		customJsonObjectRequest = new CustomJsonObjectRequest(
			Method.GET,
			url, 
			data_request, 
			new Response.Listener<JSONObject>() {	
				
				@Override
				public void onResponse(JSONObject response) {
					
					// Menentukan pengolah data yang sudah di dapat dari Django API.
					// news/user_desc/news_source
					parse_json_object(response, process_id);
					
				}
			}, new Response.ErrorListener() {	
				@Override
				public void onErrorResponse(VolleyError error) {
					VolleyLog.e(TAG, "Error: " + error.toString());
					error.printStackTrace();
					
					// Matikan Progress Dialog jika error
					if(pDialog.isShowing()) {
						pDialog.dismiss();
					}
					
					// Tampilan ERROR langsung mengubah layout.
					// Home.this.setContentView(R.layout.error_view);
					
				}
			}
		){};
				
		if(process_id == user_desc_process) {
			customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
			
			// TIMEOUT HANDLER (21 Des)
			customJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			// Adding request to request queue (Menjalankan Request)
			// TODO Lakukan pemeriksaan cache sehingga tidak dilakukan request ke server lagi.
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_user_desc_req);
			
		} else if (process_id == news_process) {
			customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
			
			// TIMEOUT HANDLER (21 Des)
			customJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_news_req);
			
		} else if (process_id == news_source_process) {
			customJsonObjectRequest.setPriority(Priority.NORMAL);
			
			// TIMEOUT HANDLER (21 Des)
			customJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_ns_req);
		}
		
	
		
	}


	private void parse_json_object(JSONObject responseJsonObject, int process_id) {
		/*
		 * Menentukan fungsi yg akan digunakan untuk masing-masing data yg diperoleh,
		 * seperti News, User_Desc, atau News_Source
		 */
    		
		switch (process_id) {
			case user_desc_process:
				if (LOG) Log.v(TAG, "1. Memparse JSON Object User Desc");
				process_user_desc_json_object(responseJsonObject);
				break;
			case news_process:
				// Proses pengolahan JSON Object sekaligus Populate ListView
				if (LOG) Log.v(TAG, "2. Memparse JSON Object News");
				process_news_and_populate_list_view(responseJsonObject);
				break;
			case news_source_process:
				if (LOG) Log.v(TAG, "3. Memparse JSON Object News Source");
				process_news_source_json_object(responseJsonObject);
				break;
				
		}    	
    	
	} //END - Parse JSONObject Data 	



	/**
	 * Olah data sumber berita (news_source).
	 * Data disimpan dalam sqlite (Jan, 7)
	 * @param responseJsonObject
	 */
	private void process_news_source_json_object(JSONObject responseJsonObject) {
		
		if (LOG) Log.v(TAG, "Proses process_news_source_json_object");
		
		try {
    		
    		//JIka header di JSON Object berisi "objects"
    		if( !responseJsonObject.isNull(api_objects) ) {
    			
    			int i = 0;
    			
	    		JSONObject jsonResponse 		= new JSONObject(responseJsonObject.toString());    		
	    		JSONArray jArray_ns_data 		= jsonResponse.getJSONArray(api_objects);  		
	    		int jArray_news_data_length 	= jArray_ns_data.length();
	    		
	    		
	    		// Bersihkan data jika tidak kosong
	    		if(!aController.get_all_news_source_list().isEmpty()) {
	    			Log.w(TAG, "Data news_source dibersihkan");
	    			aController.get_all_news_source_list().clear();
	    		}

	    		
	    		// Menyimpan data News Source ke dalam SQLite Databse (Jan, 6th)
	    		ActiveAndroid.beginTransaction();
	    		try {
					for ( i = 0; i < jArray_news_data_length; i++ ) {							
						JSONObject jsonObject = jArray_ns_data.getJSONObject(i);
						
						int news_source_id = jsonObject.getInt("id");
						String source_category = jsonObject.getString("source_category");
						String source_publisher = jsonObject.getString("source_publisher");
						String source_url = jsonObject.getString("source_url");
						
						NewsSourceTbl ns = new NewsSourceTbl(news_source_id, source_publisher, source_category, source_url);
						ns.save();
						
					}
					ActiveAndroid.setTransactionSuccessful();
	    		} finally {
	    			ActiveAndroid.endTransaction();
	    		}
				
				if (LOG) Log.i(TAG, "All news source on SQLite: "+ NewsSourceTbl.all(NewsSourceTbl.class).size());
	    		
    		} 			
			
		} catch (JSONException e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {

			// Tahap ketiga request json object.
			// Sembunyikan Progress Dialog karena dianggap tidak ada proses request API lagi.
			// Jika Progress Dialog sudah pernah tampil dan masih tampil hingga sekarang.
			if(this.pDialog_run_once && pDialog.isShowing()) {
				pDialog.dismiss();  // Sembunyikan Progress Dialog
				this.pDialog_run_once = false;  // Kondisi kembali ke awal
			}
			
			populate_sqlite_ns();
			
			
		}	
	}

	
	/**
	 * Method untuk memasukkan data sumber berita ke dalam 
	 * variable Apps.
	 */
	private void populate_sqlite_ns() {
		// Create an object of Select to issue a select query

		Select select = new Select();

		// Call select.all() to select all rows from our table which is
		// represented by Person.class and execute the query.

		// It returns an ArrayList of our Person objects where each object
		// contains data corresponding to a row of our database.

		ArrayList<NewsSourceTbl> ns_qres = select.all().from(NewsSourceTbl.class).execute();

		// Iterate through the ArrayList to get all our data. We ll simply add
		// all the data to our StringBuilder to display it inside a Toast.
		
		// Olah data news_source dari API, kemudian simpan ke Apps.
		List<News_Source> list_ns_temp = new ArrayList<News_Source>();
		for (NewsSourceTbl ns_data : ns_qres) {
			int ns_id = ns_data.ns_id;
			String source_publisher = ns_data.source_publisher;
			String source_category = ns_data.source_category;
			String source_url = ns_data.source_url;
			list_ns_temp.add(new News_Source(ns_id, source_publisher, source_category, source_url));
		}
		
		// Add All data news_source yang baru didapatkan ke Apps.
		aController.add_all_news_source_list(list_ns_temp);
		list_ns_temp.clear();  // Clear from memory	

		if (LOG) Log.i(TAG, "News Sources populated from SQLite.");

	}


	/**
	 * Olah user_desc : Infomasi user sekarang.
	 * @param userJsonObj
	 */
	private void process_user_desc_json_object(JSONObject userJsonObj) {
		
		try {
				// Karena API memberikan satu data berita string object, jadi langsung
				// ubah string object ke Java JSON Object.
	    		JSONObject jsonObject = new JSONObject(userJsonObj.toString());
	    		
				String bio = jsonObject.getString("bio");
				JSONArray news_choices = jsonObject.getJSONArray("news_choices");  // Array news_choices
				String profile_pict = jsonObject.getString("profile_pict");
				int user_desc_id = jsonObject.getInt("id");  
				JSONObject user_obj = jsonObject.getJSONObject("user");  // Object User
				
				// (news_choices) List news_source yg dipilih user
				List<News_Source> list_ns_temp = new ArrayList<News_Source>();
				for(int i = 0; i<news_choices.length(); i++) {
					JSONObject nc_object = news_choices.getJSONObject(i);  // news_choices berisi Array object news_source
					
					// Data-data news_source
					int news_source_id = nc_object.getInt("id");  
					String source_category = nc_object.getString("source_category");
					String source_publisher = nc_object.getString("source_publisher");
					String source_url = nc_object.getString("source_url");	
					
					list_ns_temp.add(new News_Source(
							news_source_id, 
							source_publisher, 
							source_category, 
							source_url)
					);				
				}
								
				// addAll list ke list aController
				aController.add_user_choices(list_ns_temp);
				list_ns_temp.clear();
				
				// Buka Choose News Source jika belum ada yg dipilih (Jan, 9)
				if(aController.get_user_choices().size() == 0) {
					Log.w(TAG, "User belum ada memilih News Source");
					startActivity(new Intent(this, ChooseNewsSource.class));
				}
				
				// Ambil Main News URL
				url_prop_news = Helper.generate_news_url_properties(aController.get_user_choices());
				
				// Parsing data object user
				int user_id = user_obj.getInt("id");
				String email = user_obj.getString("email");
				String first_name = user_obj.getString("first_name");
				String last_name = user_obj.getString("last_name");
				String username = user_obj.getString("username");
				
				// Set user_desc dan user
				aController.set_user(new User(user_id, username, email, first_name, last_name));
				aController.set_user_desc(new UserDesc(
						user_desc_id,
						aController.get_user(), 
						aController.get_all_news_source_list(), 
						bio, 
						profile_pict));
	    		
	    		
				if (LOG) Log.i(TAG, "User aktif: "+aController.get_user_desc().getUser_info().getUsername());
				if (LOG) Log.i(TAG, "Chosen news: "+url_prop_news.toString());
			
		} catch (JSONException e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
			e.printStackTrace();
		} finally {
			
			// Buat user berita dengan URL Property "?news_category=xxx"
			url_news = aController.getUrl_news(0, url_prop_news);  //0=all data. All news data with default url prop
			
			// Tambah Alamat Utama Request Berita
			aController.setMain_news_url(url_news);
			
			// (2) Mulai request data News
			request_json_object(url_news, news_process, pDialog_run_once);
		}
		
	}



	/**
	 * Mengolah data berita (JSONObject) yang diperoleh dari Django API. 
	 * Kemudian menampilkan ListView.
	 * @param resJsonObject: Json Object data berita
	 */
	private void process_news_and_populate_list_view(JSONObject resJsonObject) {
		
		try {
    		    		
    		//Jika header di JSON Object berisi "objects"
    		if( !resJsonObject.isNull(api_objects) ) {
    			
    			int i = 0;
    			
	    		JSONObject jsonResponse = new JSONObject(resJsonObject.toString());
	    		
	    		// Mendapatkan next dan prev url
	    		aController.setUrl_next_news_page(jsonResponse.getString("next"));
	    		aController.setUrl_prev_news_page(jsonResponse.getString("previous"));
	    		
	    		// Banyak berita
	    		aController.news_count = jsonResponse.getInt("count");
	    		
	    		if(aController.news_count % 10 != 0) {
	    			aController.total_page = Math.round(aController.news_count/10)+1;
	    		} else {
	    			aController.total_page = Math.round(aController.news_count/10);
	    		}
	    		
	    		// Array news items
	    		JSONArray jArray_news_data 		= jsonResponse.getJSONArray(api_objects);    		

	    		int jArray_news_data_length 	= jArray_news_data.length();
	    		int news_list_size_old 			= aController.get_all_news_list_size();
	    		
	    		int news_list_size_new = jArray_news_data_length;
	    		
	    		if(aController.getUrl_next_news_page() == null) {
	    			// Jika tidak ada lagi link url data selanjutnya
	    			is_bottom_reached = true;  // Akhir data sudah diterima
	    			Toast.makeText(Home.this, "Last news reached", Toast.LENGTH_LONG).show();
	    			ListView listView = (ListView) findViewById(R.id.listView_list_home);
	    		    listView.removeFooterView(view_load_more);
	    		    listView.addFooterView(view_list_ended, null, false);

	    		}
	    		else {
	    			// Jika data berita (aController) != data berita datang (Network)
	    			if(news_list_size_new > news_list_size_old) {		
	    				// Penambahan data dimulai dari index terakhir data lama
						for (i = news_list_size_old; i < jArray_news_data_length; i++ ) {							
							JSONObject jsonObject = jArray_news_data.getJSONObject(i);
							News news = aController.create_news_item(jsonObject);
							aController.add_to_news_list(news);
							custom_listview_adapter.add(news);
						}
						
						custom_listview_adapter.notifyDataSetChanged();
	    			
	    			} else {  // if( news_list_size_new < news_list_size_old ) 	    				
	    				// Jika data yang baru masuk lebih sedikit,
	    				// langsung tambahkan data.
	    				for (i = 0; i < news_list_size_new; i++ ) {							
							JSONObject jsonObject = jArray_news_data.getJSONObject(i);
							News news = aController.create_news_item(jsonObject);
							aController.add_to_news_list(news);
							custom_listview_adapter.add(news);
						}
	    				
	    				custom_listview_adapter.notifyDataSetChanged();
	    				
	    			}		    			
	    		}
	    		
	    		Log.i(TAG, String.format("Ukuran Data berita baru: %d\nUkuran Data Lama:%d, "
	    				+ "\nNews Count: %d, Total pages: %d, Current page: %d",
	    				news_list_size_new, news_list_size_old,
	    				aController.news_count, aController.total_page, (aController.news_page + 1)
	    				));
	    		
	    		// Matikan Progress Dialog jika selesai Parsing data
	    		// Karena sering terjadi Progress Dialog tidak tertutup.
				if(pDialog.isShowing()) {
					pDialog.dismiss();
				}
				
    		} 			
			
		} catch (Exception e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {
			
			if (aController.getUrl_next_news_page() != null) {
				// Jika next page sudah masih ada lagi, diasumsikan akhir list belum dapat.
				is_bottom_reached = false;
				
			} else {
				is_bottom_reached = true;
				Toast.makeText(Home.this, "Last Page", Toast.LENGTH_LONG).show();
			}
			
			// Menambah halamn setiap kali berhasil parsing data News
			aController.news_page += 1;
			
			if(lv_home.getHeaderViewsCount() != 0) {
				Log.d(TAG, "Header view akan dihapus.");
				lv_home.removeHeaderView(view_load_more);
			}
			
			
		}
		
	}



	// private Menu menu;
	
	//////////////////////////////////////////////////////////
	/////////////////////////OPTION MENU//////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		
		// this.menu = menu;
		if(aController.getActive_user_id() != 3) 
			menu.getItem(0).setIcon(R.drawable.happy_cuz_all_data_loaded);
		
		// Tampilan pencarian (30, No 15)
		// Get the SearchView and set the searchable configuration
	    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
	    // Assumes current activity is the searchable activity
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//	    searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
	    
	    // Suggestion
	    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				Log.i(TAG, "onQueryTextSubmit");
				return false;  // False: berhasil Emulator dan Device. True berhasil only emulator.
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				Log.i(TAG, "onQueryTextChange: " + newText);
				
				// Untuk menghindari masalah load more data karena ujung list tercapai.
				// Jadi user scrolled harus dipastikan mati.
				Home.this.user_scrolled = false;
				
				custom_listview_adapter.getFilter().filter(newText);
				return true;
			}
		});
	    
	    
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(getApplicationContext(), Setting.class));
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}	
	////////////////////////OPTION MENU - END//////////////////
}
