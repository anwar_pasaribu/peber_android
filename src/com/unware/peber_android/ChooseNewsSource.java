package com.unware.peber_android;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.unware.peber_android.R;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News_Source;

public class ChooseNewsSource extends Activity implements OnClickListener {
	
	private final String TAG = this.getClass().getSimpleName();
	
	// Apps controller utk menghimpun variabel
	private AppsController aController;
	
	private Button btn_choose;
	private ListView lv_multi_select;
	private ArrayAdapter<String> adapter;
	private String[] news_source_text;
	
	private Gson gson;

	private boolean LOG = true;
	
	private int user_id;
	private JSONObject jsonBody;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_news_source);
		
		// Initialize android widget
		initialize_widget();
		
		// Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// Object coverter to string
		gson = new Gson();
		
		// Data user aktif
		user_id = aController.get_user_desc().getId();
		
		// Sortir News Source, tidak ada duplikat lagi
		List<String> news_categories = new ArrayList<String>();
		for(News_Source ns: aController.get_all_news_source_list()) {
			news_categories.add(ns.getSource_category());
		}		
		Set<String> set = new HashSet<String>(news_categories);
		// List kategori tanpa duplikasi data
		ArrayList<String> unique_categories_list = new ArrayList<String>(set);  
		
		news_source_text = new String[unique_categories_list.size()];
		for(int i = 0; i < unique_categories_list.size(); i++) {
			news_source_text[i] = String.format("%s", unique_categories_list.get(i));  // Updated: Nov, 26
		}
		
		Log.v(TAG, "news_source_text size:"+ news_source_text.length);
		
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_multiple_choice, news_source_text);
		
		lv_multi_select.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lv_multi_select.setAdapter(adapter);
		btn_choose.setOnClickListener(this);
		
		// Check kategori berita yang sudah dpilih
		String category = null;
		for(int position = 0; position < news_source_text.length; position++){
			// Check list jika sudah dipilih user sebelumnya.
			category = news_source_text[position];
			for(News_Source user_ns: Helper.remove_ns_duplicate(aController.get_user_choices()) ) {
				if(category.equals(user_ns.getSource_category())) {
					lv_multi_select.setItemChecked(position, true);
					Log.v(TAG, category +" is already selected.");
				}
			}
		}
		
	}
	

	@Override
	public void onClick(View v) {		
		// Mengambil data dari list
		SparseBooleanArray checked = lv_multi_select.getCheckedItemPositions();
		
		// ArrayList untuk object News_Source
		ArrayList<String> selectedItems = new ArrayList<String>();
		
		for (int i = 0; i < checked.size(); i++) {			
			// Item position in adapter
			int position = checked.keyAt(i);
			
			// Add sport if it is checked i.e.) == TRUE!
			if (checked.valueAt(i)) {
				String selected_categories = lv_multi_select.getItemAtPosition(position).toString();
				selectedItems.add(selected_categories);
			}
			
		}
		
		if(!selectedItems.isEmpty()) {
			
			// Tentukan semua News Source dengan kategori yg dipilih.
			List<Integer> selected_ns_id = new ArrayList<Integer>();
			for(String str: selectedItems){
				for(News_Source ns : aController.get_all_news_source_list()) {
					if(ns.getSource_category().equals(str))
						selected_ns_id.add(ns.getId());
				}
			}
			
			try {
				// Ubah object menjadi string berformat JSON.
				jsonBody = new JSONObject(gson.toJson(new UserDescToSend(selected_ns_id)));  
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			if (LOG) Log.d(TAG, "DATA: " + jsonBody.toString());
			
			// URL diambil dari aController (URL user_desc aktif)
			// karena proses yang akan dilakukan untuk mengubah data.
			request_update_user_desc(aController.getUrl_update_user_desc(user_id), jsonBody);
			
		} else {
			Toast.makeText(getApplicationContext(), "No item selected.", Toast.LENGTH_LONG).show();
		}

	}
	
	
	private void initialize_widget() {
		lv_multi_select = (ListView) findViewById(R.id.listView_news_source);
		btn_choose = (Button) findViewById(R.id.btn_choose_news_source);
	}
	
	
	private void request_update_user_desc(String url, JSONObject data) {	
		if (LOG) Log.d(TAG, "URL: " + url);

		// OkHttp yg mendukung PATCH (Nov 19th)
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody body = RequestBody.create(JSON, data.toString());
		OkHttpClient okHttp = new OkHttpClient();
		Request request = new Request.Builder()
			.url(url)
			.patch(body)  // PATCH data
			.build();
		Call call = okHttp.newCall(request);
		
		call.enqueue(new Callback() {
			
			@Override
			public void onResponse(com.squareup.okhttp.Response response)
					throws IOException {
				try {
					if(response.isSuccessful()) {
						
						String jsonText = response.body().string();
						
						Log.i(TAG, "Response okHttp: " + jsonText);
						
						JSONObject jsonObject = new JSONObject(jsonText);
						JSONArray news_choices = jsonObject.getJSONArray("news_choices");
						
						// (news_choices) List news_source yg dipilih user
						List<News_Source> choosen_ns = new ArrayList<News_Source>();
						for(int i = 0; i<news_choices.length(); i++) {
							for(News_Source ns: aController.get_all_news_source_list()) {
								if(ns.getId() == news_choices.getInt(i)) {
									choosen_ns.add(ns);
									break;
								}
							}
						}
						
						// Buka Activity Setting jika ukuran news_choices
						// lebih besar dari 0
						if(choosen_ns.size() >= 1) {
							
							// Simpan Kategori pilihan lama
							ArrayList<String> old_choices = new ArrayList<String>();
							for(News_Source ns: aController.get_user_choices()) {
								old_choices.add(ns.getSource_category());								
							}
							
							// Ubah kategori pilihan lama dengan yang baru
							// Masukkan list news_source yg dipilih ke Apps variable
							aController.add_user_choices(choosen_ns);
							
							// Buka activity Setting dengan extra kategori lama
							Intent intent = new Intent(getApplicationContext(),
							Setting.class);
							intent.putStringArrayListExtra("old_choices", old_choices);
							startActivity(intent);
						}
						
						
					}
				} catch (IOException | JSONException e) {
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void onFailure(Request req, IOException ex) {
				Log.e(TAG, "onFailure okHttp: " + req.body().toString());
				ex.printStackTrace();
				
			}
		});
		
		
		
	}
	
	
	// Kelas untuk data yang akan dikirim melalui API
	class UserDescToSend {
		
		private List<Integer> news_choices;  // Array id news_choices
//		private String[] news_choices;  // Array id news_choices
		public UserDescToSend(List<Integer> news_choices) {
			super();
			this.news_choices = news_choices;
		}
		
		public List<Integer> getNews_choices() {
			return news_choices;
		}
		
		public void setNews_choices(List<Integer> news_choices) {
			this.news_choices = news_choices;
		}
		
		
		
	}

}
