package com.unware.peber_android;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pushbots.push.Pushbots;
import com.unware.peber_android.R;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News_Source;
import com.unware.peber_android.tables.NewsSourceTbl;

public class Setting extends Activity {

	private final String TAG = this.getClass().getSimpleName();
	private final boolean LOG = true;
	
	// Account managing (Des 20)
	private AccountManager _accountMgr = null;
	private Setting _this;
	private final String PEBER_ACCOUNT_TYPE = "com.unware.peber_android.account";
	private Account active_account = null;

	// Apps controller utk menghimpun variabel
	private AppsController aController;

	// Android widget declarations
	private ListView listView_ns_choices;
	private GridView gridView;
	private TextView textView_user_info, textView_lable_news_categories,
					 textView_lable_user_info;
	private Button button_setting_sign_in;
	private ImageButton imageButton_add_more;
	
	// GUI string
	private String lable_user_info;
	private String lable_news_category;
	
	// EditText untuk ubah IP Address
	private EditText editText_ip_address;

	private CustomGridAdapter custom_grid_adapter;
	
	// Main URL News, sebagai parameter pembanding perubahan berita pilihan user
	private String main_url = null;
	
	// News Source Pilihan, sudah distinct (Hilangkan duplikat)
	List<News_Source> distincted_user_choices = new ArrayList<News_Source>();
	
//	private TextView textView_lable_news_categories, textView_lable_user_info;
	
	// Array NS yang baru dipilih
	private ArrayList<String>  old_choices = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		_this = this;  // Context
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		// Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// String label
		lable_user_info = getString(R.string.lable_user_info).toString();
		lable_news_category = getString(R.string.lable_news_categories).toString();
		
		// Tangani extra dari Activity lain (Jan 11)
		Bundle extras = getIntent().getExtras();
		if(null != extras && getIntent().getExtras().containsKey("old_choices")) {
			old_choices.addAll(extras.getStringArrayList("old_choices"));			
		}
		
		// Proses khusus untuk halaman ini
		// 1. Inisialisasikan widget dari layout
		initialize_widget();
		
		// 2. Periksa akun peber
		get_registered_account();
		
		// Berikan kontrol untuk ubah IP
		// change_ip_address();
		
		// Pasang adapter dengan semua data news_source
		Log.i(TAG, "User choices size:" + aController.get_user_choices().size());
		distincted_user_choices = Helper.remove_ns_duplicate(aController.get_user_choices());
		custom_grid_adapter = new CustomGridAdapter(_this, distincted_user_choices);
		gridView.setAdapter(custom_grid_adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				// Tap add button
				if(id == 0) {
					startActivity(new Intent(getApplicationContext(), ChooseNewsSource.class));
				} else {
					News_Source selected_ns = (News_Source) gridView.getItemAtPosition(position);
					
					Toast.makeText(_this, selected_ns.getSource_category() + " selected.", Toast.LENGTH_LONG).show();
					
					Intent intent_detail_kamar = new Intent(getApplicationContext(), NewsByCategory.class);
					intent_detail_kamar.putExtra("ns_category", selected_ns.getSource_category());
					startActivity(intent_detail_kamar);
				}
				
				Log.i(TAG, "onItemClick, pos:"+position+", id:"+id); // Setiap klik on item
			}

		});
		
		listView_ns_choices.setAdapter(custom_grid_adapter);
		
		// Atur tampilan textView_user_info
		// dan informasi lainnya pada Setting
		set_setting_view();
		
		
		// Set tags untuk PushBots jika user baru saja masuk (Jan, 11)
		// Alasan dibuat di Setting, untuk memastikan PushBots sudah selesai 
		// dalam tahap inisialisasi
		if(aController.is_currently_signed_in) {
			if (LOG) Log.d(TAG, "Set tags push bots");
			for(News_Source new_ns: Helper.remove_ns_duplicate(aController.get_user_choices())){
				String str_tag = new_ns.getSource_category();
				String sender_id = Pushbots.sharedInstance().getSenderId();
				String app_id = Pushbots.sharedInstance().getAppId();
				
				Pushbots.sharedInstance().tag(str_tag);
				if (LOG) Log.d(TAG, "Set tags : " + str_tag);
				if (LOG) Log.d(TAG, "Sender ID : " + sender_id);
				if (LOG) Log.d(TAG, "App ID : " + app_id);
			}
			
			// Set kembali aController.is_currently_signed_in
			// karena user sudah pernah masuk dan tags sudah dipost ke PushBots server
			aController.is_currently_signed_in = false;
			
		} else {
			if (LOG) Log.d(TAG, "User sudah pernah Sign In");
		}
		
	}

	
	// Tombol tambah pada label News Category
	public void open_choose_ns(View v) {
		
		// Buka intent Choose NS
		startActivity(new Intent(getApplicationContext(), ChooseNewsSource.class));
	}
	
	
	// Menghapus duplikasi data
	public static String[] removeDuplicates(String[] texts){
	    HashSet<String> set = new HashSet<>();
	    final int len = texts.length;
	    for(int i = 0; i < len; i++){
	        set.add(texts[i]);
	    }

	    String[] new_set = new String[set.size()];
	    int i = 0;
	    for (Iterator<String> it = set.iterator(); it.hasNext();) {
	    	new_set[i++] = it.next();
	    }
	    return new_set;
	}
	
	public static ArrayList<String> removeDuplicates(List<String> nama){
	    HashSet<String> set = new HashSet<>();
	    final int len = nama.size();
	    for(int i = 0; i < len; i++){
	        set.add(nama.get(i));
	    }

	    ArrayList<String> new_set = new ArrayList<String>();
	    for (Iterator<String> it = set.iterator(); it.hasNext();) {
	    	new_set.add(it.next());
	    }
	    return new_set;
	}
	
	/**
	 * Method untuk mengolah PushBots tags. Mancakup penghapusan seluruh tag terlebih dahulu
	 * kemudian men-set tag baru yang dipilih user.
	 */
	private void populate_pushbots_tags() {
		// TODO Lakukan dengan lebih efisien
		// Kategori untuk di Untag
		List<String> category_untag = new ArrayList<String>();
		List<String> category_untag_temp = new ArrayList<String>();
		List<String> category_tag = new ArrayList<String>();
		
		old_choices = removeDuplicates(old_choices);
		
		// List kategori lama
		for(String old_cat: old_choices) {
			category_untag.add(old_cat);
			category_untag_temp.add(old_cat);
		}
		
		// List kategori baru
		for(News_Source new_ns: Helper.remove_ns_duplicate(aController.get_user_choices())){
			category_tag.add(new_ns.getSource_category());
		}
		
		// Seleksi kategori untuk di untag
		category_untag.removeAll(category_tag);		
		// Seleksi kategori yang akan di tag
		category_tag.removeAll(category_untag_temp);
		
		Log.w(TAG, "Untag size: " + category_untag.size());
		for(String s: category_untag) {
			Log.w(TAG, "Untag: " + s);
			Pushbots.sharedInstance().untag(s);
		}
		Log.i(TAG, "Tag size: " + category_tag.size());
		for(String s: category_tag) {
			Log.i(TAG, "Tag: " + s);
			Pushbots.sharedInstance().tag(s);
		}
		
		
	}


	/**
	 * Ambil data account dari Account Manager.
	 */
	private void get_registered_account() {
		try {
			_accountMgr = AccountManager.get(_this);
			Account[] accounts = _accountMgr
					.getAccountsByType(PEBER_ACCOUNT_TYPE);
			
			if(accounts.length != 0) {
				active_account = accounts[0];
				button_setting_sign_in.setText("Sign out");
			}
			
		} catch (Exception e) {
			//setMessage(e.toString());
			e.printStackTrace();
		}
	}



	private void initialize_widget() {
		
		imageButton_add_more = (ImageButton) findViewById(R.id.imageButton_add_more);
		button_setting_sign_in = (Button) findViewById(R.id.button_setting_sign_in);
		textView_user_info = (TextView) findViewById(R.id.textView_user_info);
		editText_ip_address = (EditText) findViewById(R.id.editText_ip_address);
		
		listView_ns_choices = (ListView) findViewById(R.id.listView_ns_choices);
		gridView = (GridView) findViewById(R.id.gridView1);
		
		// Label untuk bagian-bagian halaman
		// seperti "User information" dan "News Categories"
		textView_lable_news_categories = (TextView) findViewById(R.id.textView_lable_news_categories);
		textView_lable_user_info = (TextView) findViewById(R.id.textView_lable_user_info);
	}


	private void change_ip_address() {
		// Hindari keyboard terbuka sendiri
//		editText_ip_address.setFocusable(Boolean.FALSE);
//		editText_ip_address.setFocusableInTouchMode(Boolean.FALSE);
//		
//		editText_ip_address.setText(aController.getIp_address());
//		editText_ip_address.setOnKeyListener(new View.OnKeyListener() {
//			
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//				// TODO Auto-generated method stub
//				EditText editText = (EditText) findViewById(v.getId());
//				String ip_address = editText.getText().toString();
//				
//				// keyCode: 66  = enter button
//				if(keyCode == KeyEvent.KEYCODE_ENTER) {
//					aController.setIp_address(ip_address);
//					Toast.makeText(getApplicationContext(), "IP Address set to: " + ip_address, Toast.LENGTH_LONG).show();
//				} else {
//					Log.i(TAG, "v: " + editText.getText().toString());
//				}
//				
//				return false;
//			}
//		});
		
		LayoutInflater li = LayoutInflater.from(_this);
		View promptsView = li.inflate(R.layout.input_dialog, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInput);
		
		userInput.setText(aController.getIp_address());

		// set dialog message
		alertDialogBuilder
			.setCancelable(false)
			.setPositiveButton("OK",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int id) {
			    	// get user input and set it to result
			    	// edit text
			    	String ip_address = userInput.getText().toString();
			    	if(!ip_address.equals(aController.getIp_address())) {
			    		aController.setIp_address(ip_address.trim());
			    		Toast.makeText(getApplicationContext(), "IP Address set to: " + ip_address, Toast.LENGTH_LONG).show();
			    	}
			    }
			  })
			.setNegativeButton("Cancel",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			    }
			  });

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

		
	}
	

	
	@Override
	protected void onResume() {
		Log.v(TAG, "onResume called");
		super.onResume();
		
		// Untuk memeriksa apakah user ubah kategori berita pilihan.
		check_main_url();
		
	}



	/**
	 * Memeriksa apakah ada perubahan berita pilihan user. 
	 * Atau user sign out sehingga URL berita diubah menjadi
	 * default.
	 */
	private void check_main_url() {
		// Membuat Main News URL
		main_url = aController.getUrl_news(0, Helper.generate_news_url_properties(distincted_user_choices));
		
		// Ganti Apps Controller main_url jika user mengganti kategori beritanya.
		// Main URL terganti jika:
		// 1. User ubah kategori berita.
		if(!main_url.equals(aController.getMain_news_url())) {
			Log.i(TAG, "Main URL sudah berbeda.");
			aController.is_changed_main_news_url = true;
			aController.setMain_news_url(main_url);
			
			// Refresh PushBots Tags
			populate_pushbots_tags();
			
		} else {
			Log.i(TAG, "Main URL tetap.");
		}
	}

	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.v(TAG, "onRestoreInstanceState called");
		super.onRestoreInstanceState(savedInstanceState);
		String restored_main_url = savedInstanceState.getString("main_url");
		Log.i(TAG, "Restore Instance: " + restored_main_url);
	}


	private void set_setting_view() throws NullPointerException{
		String username = "Please sign in below:";
		
		if (active_account != null) {
			// Jika user sudah masuk
			// 1. Info user
			username = active_account.name.split("__")[0];
			
			// 2. Masukkan label
			textView_lable_user_info.setText(lable_user_info);
			textView_lable_news_categories.setText(lable_news_category);
			
			// 3. Tombol add more pada GridView
//			gridView.setDrawSelectorOnTop(true);
			custom_grid_adapter.add(new News_Source(0, "", "", ""));
			// Jika total data ganjil, tambah satu data kosong
			if( !((custom_grid_adapter.size() % 2) == 0) ) {
				custom_grid_adapter.add(new News_Source(2, "", "", ""));
			}
			custom_grid_adapter.notifyDataSetChanged();
			
			// Hilangkan Image Button Add More
			// jika list tidak banyak ( < 12 NS)
			if (custom_grid_adapter.size() < 10) {
				imageButton_add_more.setVisibility(View.GONE);
			} else {
				imageButton_add_more.setVisibility(View.VISIBLE);
			}
			
		} else {
			// Jika belum masuk, 
			// 1. Bersihkan grid
			custom_grid_adapter.clear();
			custom_grid_adapter.notifyDataSetChanged();
			
			// 2. Kosongkan label bagian-bagian
			textView_lable_user_info.setText("");
			textView_lable_news_categories.setText("");
			
			// 3. Hilangkan Image Button Add More
			imageButton_add_more.setVisibility(View.GONE);
			
		}
		
		textView_user_info.setText(username);
		
	}
	
	/**
	 * Tombol untuk sign out jika user sudah masuk. 
	 * Kemudian buka acitvity Sign in jika belum.
	 */
	public void open_sign_in_activity(View v) {
		String v_text = ((Button) v).getText().toString();
		
		if (v_text.equals("Sign in")) {
			// Buka Activity Sign In
			startActivity(new Intent(_this, SignIn.class));
			
		} else if (v_text.equals("Sign out")) {
			// Hapus akun peber
			// Alert user untuk hapus akun
			new AlertDialog.Builder(this)
			.setCancelable(false)
			.setMessage("\nAre you sure to sign out?\n")
			.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Lanjut proses
					Log.i(TAG, "Sure");
					boolean remove_success = _accountMgr
							.removeAccount(active_account, null, null) != null;
					if (remove_success) {
						// Set active_account menjadi null
						active_account = null;
						set_setting_view();
						
						/// Untuk menghapus perangkat dari PushBots
						Pushbots.sharedInstance().setRegStatus(false);
						Pushbots.sharedInstance().unregister();
						
						// Set kembali teks tombol menjadi "Sign in"
						button_setting_sign_in.setText("Sign in");
						
						// Jika user tekan Up Navigation
						// Ubah ID User Desc dan Next Page URL pada
						// Apps Controller untuk memuat ulang data API
						aController.setUrl_next_news_page("");
						aController.setActive_user_id(aController.DEFAULT_USER_DESC_ID);
						
						// Alternatif jika user tekan Back Button	// Hardcoded utk user Guest
						String main_url = aController.getUrl_news(0, "?news_category=Food");
						aController.setMain_news_url(main_url);
						aController.is_changed_main_news_url = true;
						
						Toast.makeText(_this, "Logged out", Toast.LENGTH_LONG).show();
					}
				}
			})
			// Batalkan proses
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Log.i(TAG, "Cancel");
					return;
					
				}
			})
			.show();
			
			
		}
	}
	
	private void clear_all_sqlite() {
		// Alert user untuk hapus akun
		new AlertDialog.Builder(this)
		.setCancelable(false)
		.setMessage("\nAre you sure to delete all local database?\n")
		.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Log.i(TAG, "Sure");
				
				// Menghapus seluruh data dalam tabel NewsSource
				NewsSourceTbl.delete(NewsSourceTbl.class);
				
				int new_size = NewsSourceTbl.all(NewsSourceTbl.class).size();
				Log.w(TAG, "UKuran data sekarang: " + new_size);
				
			}
		})
		// Batalkan proses
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Log.i(TAG, "Cancel");
				return;
				
			}
		})
		.show();
	}
	
	
	//////////////////////////////////////////////////////////
	/////////////////////////OPTION MENU//////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_clear_sqlite) {
			clear_all_sqlite();
			return true;
		} else if (id == R.id.action_change_ip_address) {
			change_ip_address();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	////////////////////////OPTION MENU - END//////////////////
	
}
