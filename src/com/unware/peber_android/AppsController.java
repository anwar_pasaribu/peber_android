package com.unware.peber_android;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.unware.peber_android.control.LruBitmapCache;
import com.unware.peber_android.model.News;
import com.unware.peber_android.model.News_Source;
import com.unware.peber_android.model.User;
import com.unware.peber_android.model.UserDesc;

/**
 * Apps Controller V.3. Updated Oct 30.
 * Apps diextend dari Sugar ORM "SugarApp" (Jan, 6th)
 * Mengubah API menggunakan DRF (Nov 19) 
 * @author Anwar Pasaribu
 *
 */
public class AppsController extends com.activeandroid.app.Application {
	
	public static final String TAG = AppsController.class.getSimpleName();
	private Context mainContext;
	private RequestQueue queue;
	private ImageLoader mImageLoader;	
	private static AppsController mInstance;
	
	// Application Account Manager (Des, 20th)
	private AccountManager _accountMgr = null;
	private final String PEBER_ACCOUNT_TYPE = "com.unware.peber_android.account";
	private Account active_account = null;
	
	//Me create this, List yang akan menampung seluruh data
	private User user;
	private UserDesc userDesc;
	private List<News> all_news_list = new ArrayList<News>();
	private List<News_Source> all_news_source_list = new ArrayList<News_Source>();
	private List<News_Source> all_user_n_choices = new ArrayList<News_Source>();
	
	// Var utk digunakan seluruh apps
	public final int DEFAULT_USER_DESC_ID = 3;
	private int active_user_id = 3;  // Nomor id user yg signed in (3=null)
	public final String api_objects = "results";
	private String ip_address = "peber.herokuapp.com"; // IP:no_port
	private String url_prop = "";
	private String url_news = "http://%s/peber_api/news/%s";
	private String url_user = "http://%s/peber_api/user/%s"; // Ambil hanya satu data user_desc
	private String url_user_desc = "http://%s/peber_api/user_desc/%d/"; // Ambil hanya satu data user_desc
	private String url_update_user_desc = "http://%s/peber_api/update_user_desc/%d/";
	private String url_news_source = "http://%s/peber_api/news_source/";
	
	// URL Next dan Previous data
	public int news_count = 0;  // Jumlah data dari API
	public int news_page = 0;  // Halaman berita termuat
	public int total_page = 0;  // Total halaman berita (Jumlah berita / 10)
	private String main_news_url = new String();  // URL untuk menentukan link berita yang pertama kali di akses.
	private String url_next_news_page = new String();
	private String url_prev_news_page = new String();
	
	// Status apakah main_news_url berubah
	public boolean is_changed_main_news_url = false;
	
	// Status apakah user baru saja Sign In (Jan 11)
	public boolean is_currently_signed_in = false;
	
	//Lokasi Scroll ListView pada Home (lv_home) (25 Des / Christmas!)
	public int listHome_lastViewedPosition = 0;
	public int listHome_topOffset = 0;	
	
	
	/* From androidhive.com -start- */
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "Application on creation");
		
		mInstance = this;
		
		// Panggil fungsi pengambil data Account Manager
		get_registered_account();
	}
	
	
	public Context getMainContext() {
		return this.mainContext;
	}
	
	public static synchronized AppsController getInstance() {
		return mInstance;
	}
	

	public RequestQueue getRequestQueue() {
		
		if (queue == null) {
			queue = Volley.newRequestQueue(getApplicationContext());
		}

		return queue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.queue,
					new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {		
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	
	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (queue != null) {
			queue.cancelAll(tag);
		}
	}	
	/* from androidhive.com -end- */
	
	
	// Ambil data dari Account Manager
	private void get_registered_account() {
		// TODO Ambil data dari Account Manager
		try {
			_accountMgr = AccountManager.get(getBaseContext());
			Account[] accounts = _accountMgr
					.getAccountsByType(PEBER_ACCOUNT_TYPE);
			
			if(accounts.length != 0) {
				active_account = accounts[0];
				active_user_id = Integer.parseInt(active_account.name.split("__")[1]);
			}
			
		} catch (Exception e) {
			//setMessage(e.toString());
			e.printStackTrace();
		}
	}
	
	
	
	//Cek koneksi internet / jaringan
	//Source : http://stackoverflow.com/questions/4238921/detect-whether-there-is-an-internet-connection-available-on-android
	public boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	// me -start- //
	
	/////////////////////////////////////////////////////////////////
	////////////////////////////////START////////////////////////////
	
	public List<News> get_all_news() {
		return all_news_list;
	}
	
	public void add_to_news_list(News news_item) {
		all_news_list.add(news_item);
	}
	
	public int get_all_news_list_size() {
		return all_news_list.size();
	}
	
	public void clear_all_news_list() {
		this.all_news_list.clear();	
	}
	
	public News get_news_at_position(int position) {
		return all_news_list.get(position);
	}
	
	public News create_news_item(JSONObject jsonObject) {
		
		News news_item_temp = null;
		try {
			
			// Olah data news_corp
			JSONObject nc_object = jsonObject.getJSONObject("news_corp");
			int news_source_id = nc_object.getInt("id");
			String source_category = nc_object.getString("source_category");
			String source_publisher = nc_object.getString("source_publisher");
			String source_url = nc_object.getString("source_url");
			
			News_Source news_corp = new News_Source(news_source_id, source_publisher, source_category, source_url);
			
			
			news_item_temp = new News(
					jsonObject.getInt("id"),
					jsonObject.getString("news_url"),
					jsonObject.getString("news_title"),
					jsonObject.getString("news_content"),
					news_corp,
					jsonObject.getString("news_summary"),
					jsonObject.getString("news_pub_date"),
					jsonObject.getString("news_image_hero")
					);
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
		return news_item_temp;
		
	}
	
	public News get_news_by_id(int news_id ) {
		
		News news = null;
		
		for(int i = 0; i< get_all_news_list_size(); i++) {
			if(get_all_news().get(i).getId() == news_id)
				news = get_all_news().get(i);
		}
		return news;
	}
	
	
	
	//News source management
	public List<News_Source> get_all_news_source_list() {
		return all_news_source_list;
	}

	public void add_all_news_source_list(List<News_Source> news_source) {		
		this.all_news_source_list.clear();
		this.all_news_source_list.addAll(news_source);
	}
	
	
	/***
	 * Sumber berita yang dipilih oleh user
	 */
	public List<News_Source> get_user_choices() {
		return all_user_n_choices;
	}

	/**
	 * Add All dengan list NS pilihan user.
	 * Mengganti berita yang dipilih oleh user.
	 * 
	 */
	public void add_user_choices(List<News_Source> news_source) {
		this.all_user_n_choices.clear();
		this.all_user_n_choices.addAll(news_source);
	}
	
	
	
	public String getUrl_user(String url_prop) {
		String base_url = this.url_user;
		
		if(!url_prop.equals("")) {
			return String.format(base_url, this.ip_address, url_prop);
		} else {
			base_url = "http://%s/peber_api/user/";
		}
				
		return String.format(base_url, this.ip_address );
	}

	public void setUrl_user(String url_user) {
		this.url_user = url_user;
	}

	public int getActive_user_id() {
		return active_user_id;
	}

	public void setActive_user_id(int active_user_id) {
		this.active_user_id = active_user_id;
	}

	// User aktif
	public User get_user() {
		return user;
	}

	public void set_user(User user) {
		this.user = user;
	}
	
	
	// User Description 
	public UserDesc get_user_desc(){
		return userDesc;
	}
	
	public void set_user_desc(UserDesc userDesc){
		this.userDesc = userDesc;
	}

	// Atur IP Address
	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public String getUrl_data() {
		return url_prop;
	}

	// "http://%s/peber_web/api/v1/news/%s"
	public String getUrl_news(int news_id, String url_props) {
		
		String url = "";  // URL yg akan dikirim
		String base_ns_url = this.url_news;  // URL dengan format string url_prop		
		
		if (news_id != 0) {
			// URL utk data news_source utk ID tertentu
			base_ns_url = "http://%s/peber_api/news/%d/";  // URL ambil data news tertentu
			url = String.format(base_ns_url, this.ip_address, news_id);
		} else {
			// URL multi data (Tidak berdasarkan id)
			url = String.format(base_ns_url, this.ip_address, url_props);
		}
		
		return url;
	}

	/**
	 * Memperoleh alamat URL API Peber User Desc. <b>http://%s/peber_api/user_desc/%d/</b>
	 * @param id_user_desc : ID Peber User Desc yang akan dibuka.
	 * @return String URL API user_desc.
	 */
	public String getUrl_user_desc(int id_user_desc) {
		String url = String.format(this.url_user_desc, this.ip_address, id_user_desc);
		return url;
	}
	
	
	// Link untuk edit news_choices pada user_desc
	// "http://%s/peber_api/update_user_desc/%d/"
	public String getUrl_update_user_desc(int id_user_desc) {
		// Jika id ditentukan
		if (id_user_desc != 0) {
			return String.format(this.url_update_user_desc, this.ip_address, id_user_desc);
		} else {
			return String.format("http://%s/peber_api/update_user_desc/", this.ip_address);
		}
		
	}

	public void setUrl_update_user_desc(String url_update_user_desc) {
		this.url_update_user_desc = url_update_user_desc;
	}
	

	// http://%s/peber_api/news_source/
	public String getUrl_news_source(int news_source_id) {
		
		String url = "";  // URL yg akan dikirim
		String base_ns_url = this.url_news_source;  // URL utk ambil semua data news_source
		
		// URL utk data news_source utk ID tertentu
		if (news_source_id != 0) {
			base_ns_url = "http://%s/peber_api/news_source/%d/";  // URL ambil data news_source tertentu
			url = String.format(base_ns_url, this.ip_address, news_source_id);
		} 
		else // URL multi data
		{
			url = String.format(base_ns_url, this.ip_address);
		}
		
		return url;
	}

	// "?format=json"
	public void setUrl_data(String url_data) {
		this.url_prop = url_data;
	}

	public void setUrl_news(String url_news) {
		
		this.url_news = url_news;
	}

	public void setUrl_user_desc(String url_user) {
		this.url_user_desc = url_user;
	}

	public void setUrl_news_source(String url_news_source) {
		this.url_news_source = url_news_source;
	}
	/////////////////////////////////////////////////////////////////
	
	
	public int getNews_count() {
		return news_count;
	}

	public void setNews_count(int news_count) {
		this.news_count = news_count;
	}

	public String getMain_news_url() {
		return main_news_url;
	}

	public void setMain_news_url(String main_news_url) {
		this.main_news_url = main_news_url;
	}

	// Next dan Prev page
	public String getUrl_next_news_page() {
		return url_next_news_page;
	}

	public void setUrl_next_news_page(String url_next_news_page) {
		this.url_next_news_page = url_next_news_page;
	}

	public String getUrl_prev_news_page() {
		return url_prev_news_page;
	}

	public void setUrl_prev_news_page(String url_prev_news_page) {
		this.url_prev_news_page = url_prev_news_page;
	}
	
	//END of AppsController
}
