package com.unware.peber_android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.unware.peber_android.control.CustomJsonObjectRequest;
import com.unware.peber_android.helper.Helper;
import com.unware.peber_android.model.News;
import com.unware.peber_android.model.News_Source;
import com.unware.peber_android.model.User;
import com.unware.peber_android.model.UserDesc;

public class NewsByCategory extends Activity {
	
	// This class stuff
	private final String TAG = this.getClass().getSimpleName();
	private NewsByCategory this_context;
	
	private ProgressDialog pDialog;
	private String tag_news_req = "tag_news_request";
	private String tag_ns_req = "tag_news_source_request";
	private String tag_user_desc_req = "tag_user_desc_req";
	
	// Navigasi News Page
	private int total_page = 0;
	private int news_page = 0;
	private int news_count = 0;
	
	//URL Utk akses data API
	private String url_news;
	private String url_news_next;
	private String url_prop_news = null;
	
	//Process ID utk menentukan data jsonArray yg akan diproses
	private final int user_desc_process = 1;
	private final int news_process = 2;
	private final int news_source_process = 3;
	
	private AppsController aController;
	private CustomJsonObjectRequest customJsonObjectRequest;
	
	// Tag utk akses data dari API
	private final String api_objects = "results";
	private boolean LOG = true;  // Tampilkan log pada console
	private boolean pDialog_run_once = false;

	// List view
	private ListView lv_news;
	private ListHomeAdapter custom_listview_adapter = null;
	private List<News> list_all_news = new ArrayList<News>();
	
	// Untuk kontrol data API
	// News Page pindah ke Apps Controller
	boolean loadingMore = false;
	private View view_load_more;
	
	// Status Scroll yg mencapai bawah
	private boolean is_bottom_reached = false;
	
	// Izinkan scroll setelah touch dimulai.
	// Karena normalnya, onScroll akan dijalankan pada onCreate
	private boolean user_scrolled = false;
	
	// String kategori berita yanga kan di proses
	private String ns_category;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this_context = this;
		super.onCreate(savedInstanceState);
		Log.v(TAG, "onCreate called");
		
		setContentView(R.layout.activity_news_by_category);
		
		// Application Controller, berisi data ter-share utk semua activity.
		aController = (AppsController) getApplicationContext();
		
		// ListView
		lv_news = (ListView) findViewById(R.id.listView_news_by_category);
		
		// Tampilan Load more data pada akhir list view
		view_load_more = ((LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.list_view_load_more, lv_news, false);
		
		//Inisialisasi Progress Dialog Box
		pDialog = new ProgressDialog(this_context);
		pDialog.setMessage("Loading data. IP: " + aController.getIp_address());
		pDialog.setCancelable(true);
		
		// Mengatur List View		
		lv_news.addFooterView(view_load_more);  // Tampilan loading 
		custom_listview_adapter = new ListHomeAdapter(this_context, list_all_news);
		lv_news.setAdapter(custom_listview_adapter);
		lv_news.setTextFilterEnabled(true);		
		populate_list_home_listener();  // Listener pada list view
		

		// Data news_id dari Halaman Setting (21 Des)
		Bundle extras = getIntent().getExtras();
		if(null != extras && getIntent().getExtras().containsKey("ns_category")) {
			// Extras yang diperoleh dari Home.java
			// Karena data dari Pushbots dianggap string
			ns_category = String.valueOf(extras.getString("ns_category"));
			
			// Ubah title Halaman
			this_context.setTitle(ns_category);
			
			// Buat user berita dengan URL Property "?news_category=xxx"
			url_news = aController.getUrl_news(0, "?news_category=" + ns_category);  //0=all data. All news data with default url prop
			
			// Mulai request data News
			request_json_object(url_news, news_process, pDialog_run_once);
			
		} else {
			startActivity(new Intent(getApplicationContext(), Home.class));
		}		
		
		
		
	}


	
	/** 
	 * Dipanggil setelah Request semua data API dan data yang respon ada.
	 */
	private void populate_list_home_listener() {
		
		lv_news.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if (LOG) Log.v(TAG, "Pos: "+position+", ID:" + id);
				
				// Kasus error ketika user tap "Loading..."
				if (id != -1) {  
					
					if (LOG) Toast.makeText(this_context, "Opening News ID: "+id, Toast.LENGTH_SHORT).show();
					
					Intent intent_detail_kamar = new Intent(getApplicationContext(), NewsDetails.class);
					intent_detail_kamar.putExtra("news_id", String.valueOf(id));
					startActivity(intent_detail_kamar);
					
				}
				
			}
		});
		
		// Bagian untuk memuat data selanjutnya (23 Nov 15)
		lv_news.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState){
				// Untuk menghindari request pada saat onCreate
				if(scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
					this_context.user_scrolled = true;
				}
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {	
				
				int lastInScreen = firstVisibleItem + visibleItemCount;	
				
				String next_page_url = this_context.url_news_next;
				
				if(lastInScreen == totalItemCount 
						&& !(loadingMore)
						&& !(is_bottom_reached)  // Kondisi awal false; bwa
						&& user_scrolled
						&& next_page_url != null) {
					
					// Ubah status is_bottom_reached supaya request dipanggil sekali.
					// Diganti lagi setelah data selesai dikelola.
					is_bottom_reached = true;
					
					// Jalankan request next_url
					if(this_context.news_page != this_context.total_page) {
						this_context.request_json_object(
							next_page_url, 
							this_context.news_process, 
							true);  // Tidak menampilkan Progres Dialog lagi.
					} else {
						// Jika tidak ada lagi link url data selanjutnya
		    			is_bottom_reached = true;  // Akhir data sudah diterima
		    			Toast.makeText(this_context, "Scroll reacheed last news.", Toast.LENGTH_LONG).show();
		    		    this_context.lv_news.removeFooterView(view_load_more);
		    		    this_context.custom_listview_adapter.add(new News(-1, "", "footer", "", null, "", "", ""));
		    		    this_context.custom_listview_adapter.notifyDataSetChanged();
					}						
						
				} 
				
			}
		});
		
		
	}



	private void request_json_object(String url, final int process_id, final boolean run_once) {	
		Log.i(TAG, "Request URL: " + url);
		
		// Tampilkan Progress Dialog jika belum pernah tampil
		// Pemanggilan pertama fungsi ini (request_json_object),
		// membuat nilai pDialog_run_once = true. Sehingga progress
		// dialog akan tetap terlihat.
		if(!run_once) {
			pDialog.show();
			this.pDialog_run_once = true;
		}
		
		// [BELUM DIPAKAI] Data yg akan dikirim ke server.
		Map<String, String> data_request = new HashMap<String, String>();
		data_request.put("id_user", "1");
		
		customJsonObjectRequest = new CustomJsonObjectRequest(
			Method.GET,
			url, 
			data_request, 
			new Response.Listener<JSONObject>() {	
				
				@Override
				public void onResponse(JSONObject response) {
					
					// Menentukan pengolah data yang sudah di dapat dari Django API.
					// news/user_desc/news_source
					parse_json_object(response, process_id);
					
				}
			}, new Response.ErrorListener() {	
				@Override
				public void onErrorResponse(VolleyError error) {
					VolleyLog.e(TAG, "Error: " + error.toString());
					error.printStackTrace();
					
					// Matikan Progress Dialog jika error
					if(pDialog.isShowing()) {
						pDialog.dismiss();
					}
					
					// Tampilan ERROR langsung mengubah layout.
					// this_context.setContentView(R.layout.error_view);
					
				}
			}
		){};
				
		if(process_id == user_desc_process) {
			customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
			// Adding request to request queue (Menjalankan Request)
			// TODO Lakukan pemeriksaan cache sehingga tidak dilakukan request ke server lagi.
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_user_desc_req);
			
		} else if (process_id == news_process) {
			customJsonObjectRequest.setPriority(Priority.IMMEDIATE);
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_news_req);
			
		} else if (process_id == news_source_process) {
			customJsonObjectRequest.setPriority(Priority.NORMAL);
			AppsController.getInstance().addToRequestQueue(customJsonObjectRequest, tag_ns_req);
		}
		
	
		
	}


	private void parse_json_object(JSONObject responseJsonObject, int process_id) {
		/*
		 * Menentukan fungsi yg akan digunakan untuk masing-masing data yg diperoleh,
		 * seperti News, User_Desc, atau News_Source
		 */
    		
		switch (process_id) {
			case user_desc_process:
				if (LOG) Log.v(TAG, "1. Memparse JSON Object User Desc");
				process_user_desc_json_object(responseJsonObject);
				break;
			case news_process:
				// Proses pengolahan JSON Object sekaligus Populate ListView
				if (LOG) Log.v(TAG, "2. Memparse JSON Object News");
				process_news_and_populate_list_view(responseJsonObject);
				break;
			case news_source_process:
				if (LOG) Log.v(TAG, "3. Memparse JSON Object News Source");
				process_news_source_json_object(responseJsonObject);
				break;
				
		}    	
    	
	} //END - Parse JSONObject Data 	



	/**
	 * Olah data sumber berita (news_source).
	 * @param responseJsonObject
	 */
	private void process_news_source_json_object(JSONObject responseJsonObject) {
		
		if (LOG) Log.v(TAG, "Proses process_news_source_json_object");
		
		try {
    		
    		//JIka header di JSON Object berisi "objects"
    		if( !responseJsonObject.isNull(api_objects) ) {
    			
    			int i = 0;
    			
	    		JSONObject jsonResponse 		= new JSONObject(responseJsonObject.toString());    		
	    		JSONArray jArray_ns_data 		= jsonResponse.getJSONArray(api_objects);  		
	    		int jArray_news_data_length 	= jArray_ns_data.length();
	    		
	    		
	    		// Bersihkan data jika tidak kosong
	    		if(!aController.get_all_news_source_list().isEmpty()) {
	    			Log.w(TAG, "Data news_source dibersihkan");
	    			aController.get_all_news_source_list().clear();
	    		}

	    		// Olah data news_source dari API, kemudian simpan ke Apps.
	    		List<News_Source> list_ns_temp = new ArrayList<News_Source>();
				for ( i = 0; i < jArray_news_data_length; i++ ) {							
					JSONObject jsonObject = jArray_ns_data.getJSONObject(i);
					
					int news_source_id = jsonObject.getInt("id");
					String source_category = jsonObject.getString("source_category");
					String source_publisher = jsonObject.getString("source_publisher");
					String source_url = jsonObject.getString("source_url");
					
					list_ns_temp.add(new News_Source(news_source_id, source_publisher, source_category, source_url));
					
				}
				
				// Add All data news_source yang baru didapatkan ke Apps.
				aController.add_all_news_source_list(list_ns_temp);
				
				if (LOG) Log.i(TAG, "All news source size: "+ aController.get_all_news_source_list().size());
	    		
    		} 			
			
		} catch (JSONException e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {

			// Tahap ketiga request json object.
			// Sembunyikan Progress Dialog karena dianggap tidak ada proses request API lagi.
			// Jika Progress Dialog sudah pernah tampil dan masih tampil hingga sekarang.
			if(this.pDialog_run_once && pDialog.isShowing()) {
				pDialog.dismiss();  // Sembunyikan Progress Dialog
				this.pDialog_run_once = false;  // Kondisi kembali ke awal
			}
			
			
		}	
	}



	/**
	 * Olah user_desc : Infomasi user sekarang.
	 * @param userJsonObj
	 */
	private void process_user_desc_json_object(JSONObject userJsonObj) {
		
		try {
				// Karena API memberikan satu data berita string object, jadi langsung
				// ubah string object ke Java JSON Object.
	    		JSONObject jsonObject = new JSONObject(userJsonObj.toString());
	    		
				String bio = jsonObject.getString("bio");
				JSONArray news_choices = jsonObject.getJSONArray("news_choices");  // Array news_choices
				String profile_pict = jsonObject.getString("profile_pict");
				int user_desc_id = jsonObject.getInt("id");  
				JSONObject user_obj = jsonObject.getJSONObject("user");  // Object User
				
				// (news_choices) List news_source yg dipilih user
				List<News_Source> list_ns_temp = new ArrayList<News_Source>();
				for(int i = 0; i<news_choices.length(); i++) {
					JSONObject nc_object = news_choices.getJSONObject(i);  // news_choices berisi Array object news_source
					
					// Data-data news_source
					int news_source_id = nc_object.getInt("id");  
					String source_category = nc_object.getString("source_category");
					String source_publisher = nc_object.getString("source_publisher");
					String source_url = nc_object.getString("source_url");	
					
					list_ns_temp.add(new News_Source(
							news_source_id, 
							source_publisher, 
							source_category, 
							source_url)
					);				
				}
				
				// addAll list ke list aController
				aController.add_user_choices(list_ns_temp);
				
				// Ambil Main News URL
				url_prop_news = Helper.generate_news_url_properties(aController.get_user_choices());
				
				// Parsing data object user
				int user_id = user_obj.getInt("id");
				String email = user_obj.getString("email");
				String first_name = user_obj.getString("first_name");
				String last_name = user_obj.getString("last_name");
				String username = user_obj.getString("username");
				
				// Set user_desc dan user
				aController.set_user(new User(user_id, username, email, first_name, last_name));
				aController.set_user_desc(new UserDesc(
						user_desc_id,
						aController.get_user(), 
						aController.get_all_news_source_list(), 
						bio, 
						profile_pict));
	    		
	    		
				if (LOG) Log.i(TAG, "User aktif: "+aController.get_user_desc().getUser_info().getUsername());
				if (LOG) Log.i(TAG, "Chosen news: "+url_prop_news.toString());
			
		} catch (JSONException e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {
			
			// Buat user berita dengan URL Property "?news_category=xxx"
			url_news = aController.getUrl_news(0, url_prop_news);  //0=all data. All news data with default url prop
			
			// Tambah Alamat Utama Request Berita
			aController.setMain_news_url(url_news);
			
			// (2) Mulai request data News
			request_json_object(url_news, news_process, pDialog_run_once);
		}
		
	}



	/**
	 * Mengolah data berita (JSONObject) yang diperoleh dari Django API. 
	 * Kemudian menampilkan ListView.
	 * @param resJsonObject: Json Object data berita
	 */
	private void process_news_and_populate_list_view(JSONObject resJsonObject) {
		
		try {
    		    		
    		//Jika header di JSON Object berisi "objects"
    		if( !resJsonObject.isNull(api_objects) ) {
    			
    			int i = 0;
    			
	    		JSONObject jsonResponse = new JSONObject(resJsonObject.toString());
	    		
	    		// Mendapatkan URL Next Page 
	    		url_news_next = jsonResponse.getString("next");
	    		
	    		// Banyak berita
	    		news_count = jsonResponse.getInt("count");
	    		
	    		if(news_count % 10 != 0) {
	    			total_page = Math.round(news_count/10)+1;
	    		} else {
	    			total_page = Math.round(news_count/10);
	    		}
	    		
	    		// Array news items
	    		JSONArray jArray_news_data 		= jsonResponse.getJSONArray(api_objects);   		
	    		int jArray_news_data_length 	= jArray_news_data.length();
	    		
				// Jika data yang baru masuk lebih sedikit,
				// langsung tambahkan data.
				for (i = 0; i < jArray_news_data_length; i++ ) {							
					JSONObject jsonObject = jArray_news_data.getJSONObject(i);
					News news = aController.create_news_item(jsonObject);
				    list_all_news.add(news);
					custom_listview_adapter.add(news);
				}
				
				custom_listview_adapter.notifyDataSetChanged();
	    				
	    		// Matikan Progress Dialog jika selesai Parsing data
	    		// Karena sering terjadi Progress Dialog tidak tertutup.
				if(pDialog.isShowing()) {
					pDialog.dismiss();
				}
				
				Log.d(TAG, "Jumlah data News by Category: " + list_all_news.size());
				
    		} 			
			
		} catch (Exception e) {
			Log.e(TAG, "Gagal : Tidak dapat mengambil data JSON saat parseJSONObject. Message : " + e.getMessage());
		} finally {
			
			// Ganti status is_bottom_reached untuk keperluan onScroll
			if (url_news_next != null) {
				// Jika next page sudah masih ada lagi, diasumsikan akhir list belum dapat.
				is_bottom_reached = false;
				
			} else {
				is_bottom_reached = true;
				Toast.makeText(this_context, "Finally Last Page", Toast.LENGTH_LONG).show();
			}
			
			// Menambah halaman setiap kali berhasil parsing data News
			news_page  += 1;
			
			if(lv_news.getHeaderViewsCount() != 0) {
				Log.d(TAG, "Header view akan dihapus.");
				lv_news.removeHeaderView(view_load_more);
			}
		}
		
	}

}

