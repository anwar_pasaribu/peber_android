package com.unware.peber_android;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.unware.peber_android.helper.Hasher;

/**
 * Daftar baru dalam sistem
 */
public class SignUp extends Activity {
	
	private String TAG = this.getClass().getSimpleName();

	// Asynctask stuff
	private SignUpTask mAuthTask = null;

	// UI references.
	private AutoCompleteTextView signup_full_name;
	private EditText signup_username, signup_password, signup_password_confirm;
	private Button button_submit_signup;
	private View mProgressView;
	private View mLoginFormView;
	
	// Key untuk hashmap
	private String key_username = "username";
	private String key_password = "password";
	private String key_email = "email";
	private String key_first_name = "first_name";
	private String key_last_name = "last_name";
	
	// String untuk dikirim ke Sign In Activity (Jan, 11)
	private String password_plain = "";
	private String username = "";
	
	// TODO Remember this!!!
	private final int[] default_n_choices = {32, 99};  // [default] Detik Food dan KOmpas Science
	
	// GUI String
	private String str_please_wait = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		
		str_please_wait = getString(R.string.lable_please_wait).toString();

		// Set up the sign up form.
		signup_full_name = (AutoCompleteTextView) findViewById(R.id.signup_full_name);
		signup_password = (EditText) findViewById(R.id.signup_password);
		signup_password_confirm = (EditText) findViewById(R.id.signup_password_confirm);
		signup_username = (EditText) findViewById(R.id.signup_username);
		
		signup_password_confirm
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							// Proses sign up
							proceed_signup();
							return true;
						}
						return false;
					}
				});

		button_submit_signup = (Button) findViewById(R.id.button_submit_signup);
		button_submit_signup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// Proses sign up
				proceed_signup();
			}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mProgressView = findViewById(R.id.login_progress);
	}


	/**
	 * Mulai proses sign up yang mencakup:
	 * 1. Tambah pada Django User. Setelah berhasil kemudian
	 * 2. Tambah pada Peber User Desc berdasarkan ID Django User yang baru dibuat. 
	 */
	@SuppressWarnings("unchecked")
	public void proceed_signup() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		signup_username.setError(null);
		signup_full_name.setError(null);
		signup_password.setError(null);
		signup_password_confirm.setError(null);

		// Store values at the time of the login attempt.
		username = signup_username.getText().toString();
		String full_name = signup_full_name.getText().toString();
		password_plain = signup_password.getText().toString();
		String password_confirm = signup_password_confirm.getText().toString();

		// Status apakah proses regiter akan dilanjutkan
		boolean cancel = false;
		View focusView = null;

		// Cek password dan confirm sudah di isi
		if (TextUtils.isEmpty(password_plain) && TextUtils.isEmpty(password_confirm) ) {
			
			signup_password.setError(getString(R.string.error_invalid_password));
			focusView = signup_password;
			cancel = true;
			
		}
		
		// Cek apakah password dan confirm sama
		if (!isPasswordValid(password_plain, password_confirm)) {
			signup_password.setError(getString(R.string.error_invalid_password));
			focusView = signup_password;
			cancel = true;
		}

		// Cek full name
		if (TextUtils.isEmpty(full_name)) {
			signup_full_name.setError(getString(R.string.error_field_required));
			focusView = signup_full_name;
			cancel = true;
		} 
		
		// Cek username
		if (TextUtils.isEmpty(username)) {
			signup_username.setError(getString(R.string.error_field_required));
			focusView = signup_username;
			cancel = true;
			
		}
		
		// Pecah full name
		String fname = "", lname = "";
		String[] full_name_pieces = full_name.split(" ", 2);
		if(full_name_pieces.length >= 2) {
			fname = full_name_pieces[0];
			lname = full_name_pieces[1];
		} else {
			fname = full_name_pieces[0];
			lname = "";
			
		}
		
		
		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			// showProgress(true);
			
			HashMap<String, String> user_map = new HashMap<String, String>();
			user_map.put(key_username, username);
			user_map.put(key_password, password_plain);
			user_map.put(key_email, "eclipse_created@mail.com");
			user_map.put(key_first_name, fname);
			user_map.put(key_last_name, lname);
			
			
			mAuthTask = new SignUpTask();
			mAuthTask.execute(user_map);
			
		}
	}
	
	private boolean request_registration(
			String username, String email, String fname, 
			String lname, String password) {
		
		// Status apakah proses request berhasil
		boolean is_success = false;
		
		int[] userdescs = new int[0];
		User user_object = new User(username, email, fname, lname, password, userdescs);
		
		String user_api_url = AppsController.getInstance().getUrl_user("");
		Gson gson = new Gson();
		JSONObject jsonBody = null;
		
		try {
			// Ubah object menjadi string berformat JSON.
			jsonBody = new JSONObject(gson.toJson(user_object)); 
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Log.i(TAG, "Data to send:" + jsonBody.toString());
		Log.i(TAG, "URL API:" + user_api_url);
		
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody json_body = RequestBody.create(JSON, jsonBody.toString());
		
		OkHttpClient client = new OkHttpClient();
		
	    Request request = new Request.Builder()
	        .url(user_api_url)
	        .post(json_body)
	        .build();
	
	    Response response;
	    
		try {
			response = client.newCall(request).execute();
			
			// Jika request tambah Django User tidak berhasil
			if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
			
			String jsonText = response.body().string();
			Log.i(TAG, "(Django User) Response okHttp: " + jsonText);
			
			JSONObject jo_new_user = new JSONObject(jsonText);			
			int new_user_id = jo_new_user.getInt("id");
			
			
			//////////////////////////////////////////////////////////////////////////////////////////////
			// Mulai request lagi untuk tambah data Peber User Desc
			UserDesc udesc = new UserDesc("Autobio from eclipse", new_user_id, default_n_choices, new int[0]);
			String url_update_user_desc = AppsController.getInstance().getUrl_update_user_desc(0);
			
			// Request lagi tambah data User Desc
			jsonBody = new JSONObject(gson.toJson(udesc)); 
			
			Log.i(TAG, "Data User Desc to send:" + jsonBody.toString());
			Log.i(TAG, "URL API:" + url_update_user_desc);
			
			json_body = RequestBody.create(JSON, jsonBody.toString());
			
			OkHttpClient client_req_user_desc = new OkHttpClient();
			
		    Request request1 = new Request.Builder()
		        .url(url_update_user_desc)
		        .post(json_body)
		        .build();
		
		    Response user_desc_response_data = client_req_user_desc.newCall(request1).execute();
			
			// Jika request Peber User Desc (kedua) tidak berhasil
			if (!user_desc_response_data.isSuccessful()) throw new IOException("Unexpected code " + user_desc_response_data);
			
			jsonText = user_desc_response_data.body().string();
			Log.i(TAG, "(User Desc) Response okHttp: " + jsonText);
			
			jo_new_user = new JSONObject(jsonText);
			
			// Ambil ID Django User pada user_desc yang beru dibuat
			int new_user_desc_user = jo_new_user.getInt("user");
			
			// Ambil ID Peber User Desc
			int new_user_desc_id = jo_new_user.getInt("id");
			
			// Update ID user aktif
			// Ubah id user desc aktif
			AppsController.getInstance().setActive_user_id(new_user_desc_id);
			
			// Buka Home, jika ID User dan ID user pada User Desc sama
			if(new_user_desc_user == new_user_id) {
				// Untuk memastikan sistem muat data dari server jika user
				// baru login.
				AppsController.getInstance().setUrl_next_news_page("");
				
				// Status request berhasil 
				is_success = true;
			}			
			
			
		} catch (IOException | JSONException e) {
			e.printStackTrace();
			is_success = false;
		}
		
		return is_success;
	}
	

	private boolean isPasswordValid(String password, String password_confirm) {
		if (password.length() > 4 && password.equals(password_confirm)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	
	// Update view pada GUI
	private void update_view_on_process(String string) {
		button_submit_signup.setText(string);
	}
	
	
	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class SignUpTask extends AsyncTask<HashMap<String, String>, Void, Boolean> {
		
		HashMap<String, String> params_map = new HashMap<String, String>();
		@SuppressWarnings("unused")
		private HashMap<String, String>[] params;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			update_view_on_process(str_please_wait);
		}



		@SuppressWarnings("unchecked")
		@Override
		protected Boolean doInBackground(HashMap<String, String>... params) {
			this.params = params;
			// Ambil string dari params
			params_map = (HashMap<String, String>)params[0];
			
			String email = params_map.get(key_email);
			String password = params_map.get(key_password);
			String username = params_map.get(key_username);
			String fname = params_map.get(key_first_name);
			String lname = params_map.get(key_last_name);
			
			// Encode Password
			Hasher hasher = new Hasher();
			password = hasher.encode(password, hasher.randomString(12));
			
			// Lakukan request menggunakan OkHttp
			boolean req_status = request_registration(username, email, fname, lname, password);

			return req_status;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				update_view_on_process("Sign Up Successfull");
				
				// Buka activity Sign In dengan data username dan password
				Intent intent = new Intent(getApplicationContext(), SignIn.class);
				intent.putExtra("password_plain", password_plain);
				intent.putExtra("username", username);
				startActivity(intent);
			} else {
				
				update_view_on_process(SignUp.this.getString(R.string.action_register));
				signup_password.setError(getString(R.string.error_incorrect_password));
				signup_password.requestFocus();
				signup_password_confirm.setError(getString(R.string.error_incorrect_password));
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	class UserDesc {
		private String bio;
		private int user;
		private int[] news_choices, read_news;
		
		public UserDesc(String bio, int user, int[] news_choices,
				int[] read_news) {
			super();
			this.bio = bio;
			this.user = user;
			this.news_choices = news_choices;
			this.read_news = read_news;
		}
		
		public String getBio() {
			return bio;
		}
		public void setBio(String bio) {
			this.bio = bio;
		}
		public int getUser() {
			return user;
		}
		public void setUser(int user) {
			this.user = user;
		}
		public int[] getNews_choices() {
			return news_choices;
		}
		public void setNews_choices(int[] news_choices) {
			this.news_choices = news_choices;
		}
		public int[] getRead_news() {
			return read_news;
		}
		public void setRead_news(int[] read_news) {
			this.read_news = read_news;
		}
		
		
		
	}
	
	class User {
		private String username, email, first_name, last_name, password;
		private int[] userdescs;

		public User(String username, String email, String first_name,
				String last_name, String password, int [] userdescs) {
			super();
			this.username = username;
			this.email = email;
			this.first_name = first_name;
			this.last_name = last_name;
			this.password = password;
			this.setUserdescs(userdescs);
		}

		public int[] getUserdescs() {
			return userdescs;
		}

		public void setUserdescs(int[] userdescs) {
			this.userdescs = userdescs;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getFirst_name() {
			return first_name;
		}

		public void setFirst_name(String first_name) {
			this.first_name = first_name;
		}

		public String getLast_name() {
			return last_name;
		}

		public void setLast_name(String last_name) {
			this.last_name = last_name;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
		
	}
}
