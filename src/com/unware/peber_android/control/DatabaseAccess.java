package com.unware.peber_android.control;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.activeandroid.ActiveAndroid;
import com.unware.peber_android.tables.NewsSourceTbl;

public class DatabaseAccess {

	public DatabaseAccess() {
	}
	
	public void save_news_source(JSONArray ns_data) throws JSONException {
		int jArray_news_data_length = ns_data.length();
		// Menyimpan data News Source ke dalam SQLite Databse (Jan, 6th)
		ActiveAndroid.beginTransaction();
		try {
			for ( int i = 0; i < jArray_news_data_length; i++ ) {							
				JSONObject jsonObject = ns_data.getJSONObject(i);
				
				int news_source_id = jsonObject.getInt("id");
				String source_category = jsonObject.getString("source_category");
				String source_publisher = jsonObject.getString("source_publisher");
				String source_url = jsonObject.getString("source_url");
				
				NewsSourceTbl ns = new NewsSourceTbl(news_source_id, source_publisher, source_category, source_url);
				ns.save();
				
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
		
	}

}
