package com.unware.peber_android.model;

public class News {
	
	private int id;
	private News_Source news_corp;
	private String news_url, news_title, news_content, news_summary, news_pub_date;
	private String news_image_hero;
	
	public News(int id, String news_url, String news_title,
			String news_content, News_Source news_corp, String news_summary,
			String news_pub_date, String news_image_hero) {
		super();
		this.id = id;
		this.news_url = news_url;
		this.news_title = news_title;
		this.news_content = news_content;
		this.news_corp = news_corp;
		this.news_summary = news_summary;
		this.news_pub_date = news_pub_date;
		this.news_image_hero = news_image_hero;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub		
		return this.news_title == null ? "Tidak ada data berita" : this.news_title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNews_url() {
		return news_url;
	}

	public void setNews_url(String news_url) {
		this.news_url = news_url;
	}

	public String getNews_title() {
		return news_title;
	}

	public void setNews_title(String news_title) {
		this.news_title = news_title;
	}

	public String getNews_content() {
		return news_content;
	}

	public void setNews_content(String news_content) {
		this.news_content = news_content;
	}

	public News_Source getNews_corp() {
		return news_corp;
	}

	public void setNews_corp(News_Source news_corp) {
		this.news_corp = news_corp;
	}

	public String getNews_summary() {
		return news_summary;
	}

	public void setNews_summary(String news_summary) {
		this.news_summary = news_summary;
	}

	public String getNews_pub_date() {
		return news_pub_date;
	}

	public void setNews_pub_date(String news_pub_date) {
		this.news_pub_date = news_pub_date;
	}

	public String getNews_image_hero() {
		return news_image_hero;
	}

	public void setNews_image_hero(String news_image_hero) {
		this.news_image_hero = news_image_hero;
	}
	
	


}
