package com.unware.peber_android.model;

public class News_Source {
	
	private int id;
	private String source_publisher, source_category, source_url;
	
	public News_Source(int id, String source_publisher, String source_category,
			String source_url) {
		super();
		this.id = id;
		this.source_publisher = source_publisher;
		this.source_category = source_category;
		this.source_url = source_url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSource_publisher() {
		return source_publisher;
	}

	public void setSource_publisher(String source_publisher) {
		this.source_publisher = source_publisher;
	}

	public String getSource_category() {
		return source_category;
	}

	public void setSource_category(String source_category) {
		this.source_category = source_category;
	}

	public String getSource_url() {
		return source_url;
	}

	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}
	
	

}
