package com.unware.peber_android.model;
import java.util.ArrayList;
import java.util.List;

import com.unware.peber_android.model.User;

public class UserDesc {
	
	private int id;
	private User user_info;
	private List<News_Source> news_choices = new ArrayList<News_Source>();
	private String bio, profile_pict;
	
	public UserDesc(int id, User user_info, List<News_Source> news_choices, String bio,
			String profile_pict) {
		super();
		this.id = id;
		this.user_info = user_info;
		this.news_choices = news_choices;
		this.bio = bio;
		this.profile_pict = profile_pict;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser_info() {
		return user_info;
	}
	public void setUser_info(User user_info) {
		this.user_info = user_info;
	}
	public List<News_Source> getNews_choices() {
		return news_choices;
	}
	public void setNews_choices(List<News_Source> news_choices) {
		this.news_choices = news_choices;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getProfile_pict() {
		return profile_pict;
	}
	public void setProfile_pict(String profile_pict) {
		this.profile_pict = profile_pict;
	}
	
	
	

}
